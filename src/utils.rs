use ::std::ptr;
use ::std::ffi::CString;

use super::ffi;
use super::ffi::{
	instance,
	queue,
	device,
	physical_device,
};

use std::ptr::null_mut;
use super::xlib_wrappers;

pub fn get_surface_capabilities(
	physical_device: &ffi::physical_device::VkPhysicalDevice,
	surface: &ffi::VkSurfaceKHR, 
) -> Result<ffi::VkSurfaceCapabilitiesKHR, ffi::VkResult> {

	println!("get_surface_capabilities");
	let mut surface_capabilities = ffi::VkSurfaceCapabilitiesKHR::default();
	let res = unsafe { 
		ffi::physical_device::vkGetPhysicalDeviceSurfaceCapabilitiesKHR(
			*physical_device,
			*surface,
			&mut surface_capabilities
		)
	};
	return if res == ffi::VkResult::VK_SUCCESS {
		Ok(surface_capabilities)
	} else {
		Err(res)
	}
}

pub fn create_swapchain_image_views(
	device: &ffi::device::VkDevice,
	cmd_buffer: &ffi::command_buffer::VkCommandBuffer,
	swapchain_images: &Vec<ffi::resource::VkImage>,
	format: ffi::VkFormat
) -> Result<Vec<ffi::resource::VkImageView>, ffi::VkResult> {

	println!("creating swapchain imageviews");
	let mut image_views: Vec<ffi::resource::VkImageView> = Vec::with_capacity(swapchain_images.len());

	let mut res = ffi::VkResult::VK_SUCCESS;

	// begin create image views
	for x in 0..swapchain_images.len() {
		println!("creating imageview {} of {}", x + 1, swapchain_images.len());
		let image = swapchain_images[x as usize];

		println!("setting image layout");
		self::set_image_layout(
			&image,
			&cmd_buffer,
			ffi::resource::VkImageAspectFlagBits::VK_IMAGE_ASPECT_COLOR_BIT as u32, 
			ffi::resource::VkImageLayout::VK_IMAGE_LAYOUT_UNDEFINED,
			ffi::resource::VkImageLayout::VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
		);

		println!("Creating VkImageViewCreateInfo");
		let image_view_info = ffi::resource::VkImageViewCreateInfo {
			sType: ffi::VkStructureType::VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
			pNext: null_mut(),
			format: format,
			components: ffi::VkComponentMapping {
				r: ffi::VkComponentSwizzle::VK_COMPONENT_SWIZZLE_R,
				g: ffi::VkComponentSwizzle::VK_COMPONENT_SWIZZLE_G,
				b: ffi::VkComponentSwizzle::VK_COMPONENT_SWIZZLE_B,
				a: ffi::VkComponentSwizzle::VK_COMPONENT_SWIZZLE_A,
			},
			subresourceRange: ffi::resource::VkImageSubresourceRange {
				baseArrayLayer: 0,
				layerCount: 1,
				baseMipLevel: 0,
				aspectMask: ffi::resource::VkImageAspectFlagBits::VK_IMAGE_ASPECT_COLOR_BIT as u32,
				levelCount: 1
			},
			image: image,
			viewType: ffi::resource::VkImageViewType::VK_IMAGE_VIEW_TYPE_2D,
			flags: 0
		};

		println!("Calling vkCreateImageView");
		let mut image_view: ffi::resource::VkImageView = null_mut();
		res = unsafe {
			ffi::device::vkCreateImageView(*device, &image_view_info, null_mut(), &mut image_view)
		};

		println!("vkCreateImageView result: {:?}", res);
		if res != ffi::VkResult::VK_SUCCESS {
			break;
		} else {
			image_views.push( image_view );
		}
	}
	
	if res == ffi::VkResult::VK_SUCCESS {
		Ok(image_views)
	} else {
		Err(res)
	}
}


pub fn create_swapchain_khr(
	device: &ffi::device::VkDevice,
	surface: &ffi::VkSurfaceKHR,
	format: ffi::VkFormat,
	swap_chain_count: u32
) -> Result<ffi::VkSwapchainKHR, ffi::VkResult> {
	
	// begin create_swapchain(image_count)
	let mut extent = ffi::VkExtent2D::default();
	extent.width = 800;
	extent.height = 600;

	let swapchain_info = ffi::VkSwapchainCreateInfoKHR {
		sType: ffi::VkStructureType::VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
		pNext: null_mut(),
		surface: *surface,
		minImageCount: swap_chain_count,
		imageFormat: format, // TODO: detect preferred format
		imageExtent: extent,
		preTransform: ffi::VkSurfaceTransformFlagBitsKHR::VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR,//surface_capabilities.currentTransform,
		compositeAlpha: ffi::VkCompositeAlphaFlagBitsKHR::VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
		imageArrayLayers: 1,
		presentMode: ffi::VkPresentModeKHR::VK_PRESENT_MODE_FIFO_KHR, // todo: detect VK_PRESENT_MODE_IMMEDIATE_KHR
		oldSwapchain: null_mut(),
		clipped: ffi::VK_TRUE,
		imageColorSpace: ffi::VkColorSpaceKHR::VK_COLORSPACE_SRGB_NONLINEAR_KHR,
		imageUsage: ffi::resource::VkImageUsageFlagBits::VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT as u32,
		imageSharingMode: ffi::VkSharingMode::VK_SHARING_MODE_EXCLUSIVE,
		queueFamilyIndexCount: 0,
		pQueueFamilyIndices: null_mut(),
		flags:0
	};
	let mut swapchain: ffi::VkSwapchainKHR = null_mut();
	let res = unsafe {
		ffi::device::vkCreateSwapchainKHR(*device, &swapchain_info, null_mut(), &mut swapchain)
	};
	println!("create swapchain res: {:?}", res);
	return if res == ffi::VkResult::VK_SUCCESS {
		Ok(swapchain)
	} else  {
		Err(res)
	}
}

pub fn get_swapchain_images_khr(
	device: &ffi::device::VkDevice,
	swapchain: &ffi::VkSwapchainKHR
) -> Result<Vec<ffi::resource::VkImage>, ffi::VkResult> {

	println!("get swapchain images");
	let mut swap_chain_count = 0;
	let mut res = unsafe {
		ffi::device::vkGetSwapchainImagesKHR(*device, *swapchain, &mut swap_chain_count, null_mut())
	};
	println!("vkGetSwapchainImagesKHR res : {:?}, swapchain count: {}", res, swap_chain_count);
	if res != ffi::VkResult::VK_SUCCESS {
		return Err(res)
	}
	let mut swapchain_images: Vec<ffi::resource::VkImage> = Vec::with_capacity(swap_chain_count as usize);
	let data = swapchain_images.as_mut_ptr();
	let cap = swapchain_images.capacity();
	unsafe {
		println!("!");
		res = ffi::device::vkGetSwapchainImagesKHR(*device, *swapchain, &mut swap_chain_count, data);
		println!("!!");
		swapchain_images = Vec::from_raw_parts(data, swap_chain_count as usize, cap);
	};
	if res == ffi::VkResult::VK_SUCCESS {
		Ok(swapchain_images)
	} else {
		Err(res)
	}
}

pub fn get_device_queue(
	device: &ffi::device::VkDevice,
	graphics_queue_family_index: u32
) -> ffi::queue::VkQueue  {

	let mut queue: ffi::queue::VkQueue = null_mut();
	unsafe {
		ffi::device::vkGetDeviceQueue(*device, graphics_queue_family_index, 0, &mut queue)
	};
	return queue;
}

pub fn set_image_layout(
	image: &ffi::resource::VkImage,
	buffer: &ffi::command_buffer::VkCommandBuffer, 
	aspect_mask: ffi::resource::VkImageAspectFlags,
	old_image_layout: ffi::resource::VkImageLayout,
	new_image_layout: ffi::resource::VkImageLayout) {

	let mut image_memory_barrier = ffi::memory::VkImageMemoryBarrier::default();
	image_memory_barrier.sType = ffi::VkStructureType::VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
	image_memory_barrier.pNext = null_mut();
	image_memory_barrier.srcAccessMask = 0;
	image_memory_barrier.dstAccessMask = 0;
	image_memory_barrier.oldLayout = old_image_layout;
	image_memory_barrier.newLayout = new_image_layout;
	image_memory_barrier.image = *image;
	image_memory_barrier.subresourceRange.aspectMask = aspect_mask;
	image_memory_barrier.subresourceRange.baseMipLevel = 0;
	image_memory_barrier.subresourceRange.levelCount = 1;
	image_memory_barrier.subresourceRange.layerCount = 1;

	if old_image_layout == ffi::resource::VkImageLayout::VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL {
		image_memory_barrier.srcAccessMask = ffi::VkAccessFlagBits::VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT as u32;
	}

	if new_image_layout == ffi::resource::VkImageLayout::VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL {
		image_memory_barrier.dstAccessMask = ffi::VkAccessFlagBits::VK_ACCESS_TRANSFER_WRITE_BIT as u32;
	}

	if new_image_layout == ffi::resource::VkImageLayout::VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL {
		image_memory_barrier.dstAccessMask = ffi::VkAccessFlagBits::VK_ACCESS_TRANSFER_READ_BIT as u32;
	}

	if old_image_layout == ffi::resource::VkImageLayout::VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL {
		image_memory_barrier.srcAccessMask = ffi::VkAccessFlagBits::VK_ACCESS_TRANSFER_WRITE_BIT as u32;
	}

	if new_image_layout == ffi::resource::VkImageLayout::VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL {
		image_memory_barrier.srcAccessMask = ffi::VkAccessFlagBits::VK_ACCESS_HOST_WRITE_BIT as u32| ffi::VkAccessFlagBits::VK_ACCESS_TRANSFER_WRITE_BIT as u32;
		image_memory_barrier.dstAccessMask = ffi::VkAccessFlagBits::VK_ACCESS_SHADER_READ_BIT as u32;
	}

	if new_image_layout == ffi::resource::VkImageLayout::VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL {
		image_memory_barrier.dstAccessMask = ffi::VkAccessFlagBits::VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT as u32
	}

	if new_image_layout == ffi::resource::VkImageLayout::VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL {
		image_memory_barrier.dstAccessMask = ffi::VkAccessFlagBits::VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT as u32;
	}

	let src_stages = ffi::VkPipelineStageFlagBits::VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT as u32;
	let dest_stages = ffi::VkPipelineStageFlagBits::VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT as u32;

	unsafe {
		ffi::command_buffer::vkCmdPipelineBarrier(*buffer, src_stages, dest_stages, 0, 0, null_mut(), 0, null_mut(), 1, &image_memory_barrier);
	};
}

pub fn execute_begin_command_buffer(buffer: &ffi::command_buffer::VkCommandBuffer) {
	let begin_info = ffi::command_buffer::VkCommandBufferBeginInfo {
		sType: ffi::VkStructureType::VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
		pNext: null_mut(),
		flags: 0,
		pInheritanceInfo: null_mut()
	};

	let res = unsafe {
		ffi::command_buffer::vkBeginCommandBuffer(*buffer, &begin_info)
	};

	assert!(res == ffi::VkResult::VK_SUCCESS);
}

pub fn execute_end_command_buffer(buffer: &ffi::command_buffer::VkCommandBuffer) {
	let res = unsafe {
		ffi::command_buffer::vkEndCommandBuffer(*buffer)
	};
	assert!(res == ffi::VkResult::VK_SUCCESS);
}

pub fn execute_queue_command_buffer(
	device: &ffi::device::VkDevice,
	queue: &ffi::queue::VkQueue,
	buffers: &Vec<ffi::command_buffer::VkCommandBuffer>
) {
	let fence_info = ffi::VkFenceCreateInfo {
		sType: ffi::VkStructureType::VK_STRUCTURE_TYPE_FENCE_CREATE_INFO,
		pNext: null_mut(),
		flags: 0
	};
	let mut fence: ffi::VkFence = null_mut();
	let mut res = unsafe {
		ffi::device::vkCreateFence(*device, &fence_info, null_mut(), &mut fence)
	};
	assert!(res == ffi::VkResult::VK_SUCCESS);

	let pflags = ffi::VkPipelineStageFlagBits::VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT as u32;

	let submit_info = ffi::VkSubmitInfo {
		sType: ffi::VkStructureType::VK_STRUCTURE_TYPE_SUBMIT_INFO,
		pNext: null_mut(),
		waitSemaphoreCount: 0,
		pWaitSemaphores: null_mut(),
		pWaitDstStageMask: &pflags,
		commandBufferCount: 1,
		pCommandBuffers: buffers.as_ptr(),
		signalSemaphoreCount: 0,
		pSignalSemaphores: null_mut()
	};

	res = unsafe {
		ffi::queue::vkQueueSubmit(*queue, 1, &submit_info, fence) 
	};
	assert!(res == ffi::VkResult::VK_SUCCESS);
	loop {
		res = unsafe {
			ffi::device::vkWaitForFences(*device, 1, &fence, ffi::VK_TRUE, 1_000_000_000)
		};
		if res != ffi::VkResult::VK_TIMEOUT {
			break;
		}
	}
	assert!(res == ffi::VkResult::VK_SUCCESS);
	unsafe {
		ffi::device::vkDestroyFence(*device, fence, null_mut());
	};
}

// TODO: take memory management out of the picture for the api here.
// We should operate on Borrows, which can be out of boxes or not. It
// should be up to the caller.
pub fn free_command_buffers(
	device: &Box<ffi::device::VkDevice>,
	cmd_pool: &ffi::command_buffer::VkCommandPool,
  cmd_buffers: &Vec<ffi::command_buffer::VkCommandBuffer>
) {
	unsafe {
		ffi::device::vkFreeCommandBuffers(**device, *cmd_pool, cmd_buffers.len() as u32, cmd_buffers.as_ptr());
	};
}

pub fn destroy_command_pool(
	device: &Box<ffi::device::VkDevice>,
	cmd_pool: &ffi::command_buffer::VkCommandPool,
) {
	unsafe {
		ffi::device::vkDestroyCommandPool(**device, *cmd_pool, null_mut());
	}
}

pub fn create_command_pool(
	device: &Box<ffi::device::VkDevice>,
  queue_family_index: u32
) -> Result<ffi::command_buffer::VkCommandPool, ffi::VkResult> {
	//command buffer
	let mut pool_info = ffi::command_buffer::VkCommandPoolCreateInfo::default();
	pool_info.sType = ffi::VkStructureType::VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	pool_info.pNext = null_mut();
	pool_info.queueFamilyIndex = queue_family_index; // we will want to properly detect this in the future, but for now it's coupled into utils::create_device()
	pool_info.flags = 0;

	let mut cmd_pool: ffi::command_buffer::VkCommandPool = null_mut();
	let res;
	unsafe {
		res =
			ffi::device::vkCreateCommandPool(**device, &pool_info, null_mut(), &mut cmd_pool);
	}

	if res == ffi::VkResult::VK_SUCCESS {
		Ok(cmd_pool)
	} else {
		Err(res)
	}
}

pub fn create_command_buffer(
	device: &Box<ffi::device::VkDevice>,
	pool: &ffi::command_buffer::VkCommandPool
) -> Result<ffi::command_buffer::VkCommandBuffer, ffi::VkResult> {

	let cmd_buffer_info = ffi::command_buffer::VkCommandBufferAllocateInfo {
		sType: ffi::VkStructureType::VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
		pNext: null_mut(),
		commandPool: *pool,
		level: ffi::command_buffer::VkCommandBufferLevel::VK_COMMAND_BUFFER_LEVEL_PRIMARY,
		commandBufferCount: 1
	};

	let mut cmd_buffer:ffi::command_buffer::VkCommandBuffer = null_mut();
	let res = unsafe {
			ffi::device::vkAllocateCommandBuffers(**device, &cmd_buffer_info, &mut cmd_buffer)
	};

	if res == ffi::VkResult::VK_SUCCESS {
		Ok(cmd_buffer)
	} else {
		Err(res)
	}
}

pub fn physical_device_surface_supports_present(
	physical_device: &ffi::physical_device::VkPhysicalDevice,
	surface : &ffi::VkSurfaceKHR
	) -> bool {
	let mut supports_present: ffi::VkBool32 = 0;
	unsafe{
		physical_device::vkGetPhysicalDeviceSurfaceSupportKHR(*physical_device, 0, *surface, &mut supports_present);
	};
	(supports_present == ffi::VK_TRUE)
}

pub fn get_physical_device_formats_khr(
	pd: &ffi::physical_device::VkPhysicalDevice,
	surface : &ffi::VkSurfaceKHR
	) -> Vec<ffi::VkSurfaceFormatKHR> {
	let mut format_count = 0;
	unsafe {
		ffi::physical_device::vkGetPhysicalDeviceSurfaceFormatsKHR(*pd, *surface, &mut format_count, null_mut());
	};
	let mut formats = Vec::with_capacity(format_count as usize);
	let ptr = formats.as_mut_ptr();
	let cap = formats.capacity();
	unsafe {
		ffi::physical_device::vkGetPhysicalDeviceSurfaceFormatsKHR(*pd, *surface, &mut format_count, ptr);
		formats = Vec::from_raw_parts(ptr, format_count as usize, cap);
	};
	formats	
}

pub fn get_physical_device_memory_properties(
	pd: &ffi::physical_device::VkPhysicalDevice,
	) -> ffi::physical_device::VkPhysicalDeviceMemoryProperties {
	let mut memory_properties= ffi::physical_device::VkPhysicalDeviceMemoryProperties::default();
	unsafe {
		ffi::physical_device::vkGetPhysicalDeviceMemoryProperties(*pd, &mut memory_properties);
	};
	memory_properties
}

pub fn get_physical_device_properties(
	pd: &ffi::physical_device::VkPhysicalDevice,
	) -> ffi::physical_device::VkPhysicalDeviceProperties {
	let mut properties= ffi::physical_device::VkPhysicalDeviceProperties::default();
	unsafe {
		ffi::physical_device::vkGetPhysicalDeviceProperties(*pd, &mut properties);
	};
	properties
}

pub fn create_xlib_surface_khr(
	instance: &Box<ffi::instance::VkInstance>,
	xlib_window: &xlib_wrappers::XlibWindow
) -> Result<ffi::VkSurfaceKHR, ffi::VkResult> {

	let sinfo = ffi::khr::VkXlibSurfaceCreateInfoKHR {
		sType : ffi::VkStructureType::VK_STRUCTURE_TYPE_XLIB_SURFACE_CREATE_INFO_KHR,
		pNext : null_mut(),
		flags : ffi::VkFlags::default(),
		dpy : xlib_window.display,
		window : xlib_window.window
	};

	let mut surface: ffi::VkSurfaceKHR = null_mut();

	let res = unsafe { ffi::instance::vkCreateXlibSurfaceKHR(
		**instance, 
		&sinfo,
		null_mut(),
		&mut surface //out
		)
	};

	if res == ffi::VkResult::VK_SUCCESS {
		Ok(surface)
	} else {
		Err(res)
	}
}

pub fn instance_layer_properties() -> Result<Vec<ffi::VkLayerProperties>, ffi::VkResult>{
	let mut layer_count = 0;
	let mut vk_props:Vec<ffi::VkLayerProperties>;
	let mut res: ffi::VkResult;

	loop { 
		res = unsafe { ffi::vkEnumerateInstanceLayerProperties(&mut layer_count, ptr::null_mut()) };
		vk_props = Vec::with_capacity(layer_count as usize);
		if res != ffi::VkResult::VK_INCOMPLETE &&
			 res != ffi::VkResult::VK_SUCCESS {
				 break;
		}
		let raw = vk_props.as_mut_ptr();
		let cap = vk_props.capacity();
		unsafe { 
			res = ffi::vkEnumerateInstanceLayerProperties(&mut layer_count, raw);
			vk_props = Vec::from_raw_parts(raw, layer_count as usize, cap);
		}

		let incomplete = res == ffi::VkResult::VK_INCOMPLETE;
		if !incomplete { break; }
	}
	return if res == ffi::VkResult::VK_SUCCESS {
		Ok(vk_props)
	} else {
		Err(res)
	}
}

pub fn instance_extension_properties() -> Result<Vec<ffi::VkExtensionProperties>, ffi::VkResult> {
	let mut instance_extension_count = 0;
	let mut extension_props:Vec<ffi::VkExtensionProperties>;
	let mut res: ffi::VkResult;

	loop {
		res = unsafe {
			ffi::vkEnumerateInstanceExtensionProperties(
				ptr::null(),
				&mut instance_extension_count,
				ptr::null_mut()
				)
		};
		extension_props = Vec::with_capacity(instance_extension_count as usize);
		if res != ffi::VkResult::VK_INCOMPLETE &&
			 res != ffi::VkResult::VK_SUCCESS {
				break;
		}
		let p = extension_props.as_mut_ptr();
		let cap = extension_props.capacity();
		unsafe {
			res = ffi::vkEnumerateInstanceExtensionProperties(
				ptr::null(),
				&mut instance_extension_count,
				p
				);
			extension_props = Vec::from_raw_parts(p, instance_extension_count as usize, cap);
		};
		if res != ffi::VkResult::VK_INCOMPLETE { break; }
	}
	return if res == ffi::VkResult::VK_SUCCESS {
		Ok(extension_props)
	} else {
		Err(res)
	}
}

pub fn init_instance(
	app_short_name: &'static str,
	enabled_layer_names: Vec<&'static str>,
	enabled_extension_names: Vec<&'static str>
	) -> Result<Box<instance::VkInstance>, ffi::VkResult> {

	let layer_count = enabled_layer_names.len();
	let mut c_layernames = Vec::with_capacity(layer_count);
	for x in enabled_layer_names {
		println!("Layer: {}", x);
		let cstr = CString::new(&*x).unwrap();
		let ptr = cstr.into_raw() as *const ::std::os::raw::c_char;
		c_layernames.push(ptr);
	}
	let extension_count = enabled_extension_names.len();
	let mut c_extnames = Vec::with_capacity(extension_count);
	for x in enabled_extension_names {
		println!("Extension: {}", x);
		let cstr = CString::new(&*x).unwrap();
		let ptr = cstr.into_raw() as *const ::std::os::raw::c_char;
		c_extnames.push(ptr);
	}
	let mut app_info = ffi::VkApplicationInfo::default();
	app_info.sType = ffi::VkStructureType::VK_STRUCTURE_TYPE_APPLICATION_INFO;
	app_info.pNext = ptr::null_mut();
	app_info.pApplicationName = cstr(app_short_name);
	app_info.applicationVersion = 1;
	app_info.pEngineName = cstr(app_short_name);
	app_info.engineVersion = 1;
	app_info.apiVersion = ffi::VK_API_VERSION;

	let mut instance_info = instance::VkInstanceCreateInfo::default();
	instance_info.sType = ffi::VkStructureType::VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	instance_info.pNext = ptr::null();
	instance_info.enabledLayerCount = c_layernames.len() as u32;

	instance_info.ppEnabledLayerNames = if c_layernames.len() > 0 {
		c_layernames.as_ptr()
	} else {
		ptr::null_mut()
	};

	instance_info.enabledExtensionCount = c_extnames.len() as u32;

	instance_info.ppEnabledExtensionNames = if c_extnames.len() > 0 {
		c_extnames.as_ptr()
	} else {
		ptr::null_mut()
	};

	let mut instance: ffi::instance::VkInstance = ptr::null_mut();
	let res: ffi::VkResult = unsafe {
		instance::vkCreateInstance(&instance_info, ptr::null(), &mut instance)
	}; 

	return if res == ffi::VkResult::VK_SUCCESS {
		Ok(Box::new(instance))
	} else {
		Err(res)
	}

}

pub fn destroy_instance(instance: &Box<instance::VkInstance>) {
	unsafe { instance::vkDestroyInstance(**instance, ptr::null() ) };
}

pub fn enumerate_physical_devices(instance: &Box<instance::VkInstance>) -> Vec<physical_device::VkPhysicalDevice> {
	let mut gpu_count = 0;
	unsafe {
		instance::vkEnumeratePhysicalDevices(**instance, &mut gpu_count, ptr::null_mut())
	};
	let mut gpus: Vec<ffi::physical_device::VkPhysicalDevice> = Vec::with_capacity(gpu_count as usize);
	let data = gpus.as_mut_ptr();
	let cap = gpus.capacity();
	unsafe {
		::std::mem::forget(gpus);
		let res = instance::vkEnumeratePhysicalDevices(**instance, &mut gpu_count, data);
		assert!(res as i32 == ffi::VkResult::VK_SUCCESS as i32);
		gpus = Vec::from_raw_parts(data, gpu_count as usize, cap);
	};
	gpus
}

pub fn destroy_device(device: &Box<device::VkDevice>) {
	unsafe { device::vkDestroyDevice(**device, ptr::null()) };
}

pub fn enumerate_queue_family_properties(
	physical_device: &physical_device::VkPhysicalDevice
) -> Vec<queue::VkQueueFamilyProperties> {
	let mut queue_count = 0u32;
	unsafe {
		physical_device::vkGetPhysicalDeviceQueueFamilyProperties(*physical_device, &mut queue_count, ptr::null_mut());
	}
	let mut queue_props: Vec<queue::VkQueueFamilyProperties> = Vec::with_capacity(queue_count as usize);
	let data = queue_props.as_mut_ptr();
	let cap = queue_props.capacity();
	unsafe {
		physical_device::vkGetPhysicalDeviceQueueFamilyProperties(*physical_device, &mut queue_count, data);
		queue_props = Vec::from_raw_parts(data, queue_count as usize, cap);
	}
	queue_props
}

pub fn init_device_queue_create_info(
	queue_props: &Vec<queue::VkQueueFamilyProperties>
	) -> device::VkDeviceQueueCreateInfo {
	let mut queue_info = device::VkDeviceQueueCreateInfo::default();
	let mut found = false;
	let mut i = 0;
	println!("Found {} queue properties.", queue_props.len());
	for q in queue_props {
		if (q.queueFlags as u32) & (queue::VkQueueFlagBits::VK_QUEUE_GRAPHICS_BIT as u32) > 0 {
			queue_info.queueFamilyIndex = i; 
			found = true;
			break;
		}
		i += 1;
	}
	assert!(found);

	let queue_priorities = &[0.0f32] as *const f32;
	queue_info.sType = ffi::VkStructureType::VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
	queue_info.pNext = ptr::null();
	queue_info.queueCount = 1;
	queue_info.pQueuePriorities = queue_priorities;
	queue_info
}

// TODO: split queue_props out of this monolithic chunk of a function
pub fn create_device(
	physical_device: &physical_device::VkPhysicalDevice
	) -> Result<Box<device::VkDevice>, ffi::VkResult> {

	let queue_props = enumerate_queue_family_properties(&physical_device);
	let queue_info = init_device_queue_create_info(&queue_props);
	let mut device_info = ffi::device::VkDeviceCreateInfo::default();

	device_info.sType = ffi::VkStructureType::VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
	device_info.pNext = ptr::null();
	device_info.queueCreateInfoCount = 1;
	device_info.pQueueCreateInfos = &queue_info;
	device_info.enabledExtensionCount = 0;
	device_info.ppEnabledExtensionNames = ptr::null();
	device_info.enabledLayerCount = 0;
	device_info.ppEnabledLayerNames = ptr::null();
	device_info.pEnabledFeatures = ptr::null();

	let mut device: device::VkDevice = ptr::null_mut();
	let res = unsafe {
		ffi::physical_device::vkCreateDevice(*physical_device, &device_info, ptr::null(), &mut device)
	};
	return if res as i32 == ffi::VkResult::VK_SUCCESS as i32 {
		Ok(Box::new(device))
	} else {
		Err(res)
	}
}

pub fn cstr(rstr: &'static str) -> *const ::std::os::raw::c_char {
	unsafe {
		let ptr: *const i8 = ::std::mem::transmute( rstr.as_ptr() );
		ptr
	}
}

/* or something... how can we make this easier? - impl in examples
	 pub fn rstr(cstr: *const ::std::os::raw::c_char) -> ::std::string::String {
// &[i8;256] -> &[u8;256] -> &str
let s: &[u8] = unsafe { ::std::mem::transmute(&cstr) };
let ss = ::std::str::from_utf8(s);
String::from(ss.unwrap())
}
*/
