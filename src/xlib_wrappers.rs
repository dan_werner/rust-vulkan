extern crate x11_dl;

use self::x11_dl::xlib;
use std::ffi::CString;
use std::mem::zeroed;

use std::ptr::{
	null,
	null_mut
};

use ::std::os::raw::c_uint;

pub struct XlibWindow {
	pub display : *mut xlib::Display,
	pub window: xlib::Window,
	xlib: xlib::Xlib,
	wm_protocols: xlib::Atom,
	wm_delete_window: xlib::Atom,
}

impl XlibWindow {
	pub fn create(width: u32, height:u32, title:&'static str) -> Box<XlibWindow> {

		let xlib = xlib::Xlib::open().unwrap();
		let display: *mut xlib::Display = unsafe { (xlib.XOpenDisplay)(null()) };
		if display == null_mut() {
			panic!("can't open display");
		}

		let wm_delete_window_str = CString::new("WM_DELETE_WINDOW").unwrap();
		let wm_protocols_str = CString::new("WM_PROTOCOLS").unwrap();

		let wm_delete_window: xlib::Atom = unsafe { (xlib.XInternAtom)(display, wm_delete_window_str.as_ptr(), xlib::False) };
		let wm_protocols: xlib::Atom = unsafe { (xlib.XInternAtom)(display, wm_protocols_str.as_ptr(), xlib::False) };

		if wm_delete_window == 0 || wm_protocols == 0 {
			panic!("Can't load atoms.");
		}

		let screen_num = unsafe { (xlib.XDefaultScreen)(display) };
		let root = unsafe { (xlib.XRootWindow)(display, screen_num) };
		let pixel_color = unsafe{ (xlib.XBlackPixel)(display, screen_num) };
		let visualinfo = XlibWindow::get_visual_info(&xlib, display, screen_num);

		let mut attributes: xlib::XSetWindowAttributes = unsafe { zeroed() };
		attributes.background_pixel = pixel_color;
		attributes.colormap = unsafe { (xlib.XCreateColormap)( display, root, visualinfo.visual, xlib::AllocNone) };
		attributes.event_mask = xlib::ExposureMask | xlib::KeyPressMask;

		let window: xlib::Window = unsafe { 
			(xlib.XCreateWindow)(
				display,
				root,
				50, 50, // x,y
				width, height,
				0,
				visualinfo.depth, 
				xlib::InputOutput as c_uint,
				visualinfo.visual, // used as the drawable?
				xlib::CWColormap | xlib::CWEventMask | xlib::CWBackPixmap | xlib::CWBorderPixel,
				&mut attributes
			)
		};

		let title_str = CString::new(title).unwrap();

		unsafe{ (xlib.XStoreName)(display, window, title_str.into_raw()) };
		let mut protocols = [wm_delete_window];

		if unsafe { 
			(xlib.XSetWMProtocols)(display, window, &mut protocols[0] as *mut xlib::Atom, 1) 
		} == xlib::False {
			panic!("Cant set wm protocols");
		}

		let mut root_return = 0;
		let mut x_return = 0;
		let mut y_return = 0;
		let mut width_return = 0;
		let mut height_return = 0;
		let mut border_return = 0;
		let mut depth_return = 0;

		// unless we call XGetGeometry, we end up getting a BadDrawable -> XGetGeometry...
		let _ = unsafe {
			(xlib.XGetGeometry)(display, window, &mut root_return, &mut x_return, &mut y_return, &mut width_return, &mut height_return, &mut border_return, &mut depth_return)
		};

		println!("XGetGeometry: {} {} {} {} {} {}", x_return, y_return, width_return, height_return, border_return, depth_return);

		Box::new(XlibWindow{ 
			display: display, 
			window:window, 
			xlib: xlib,
			wm_protocols: wm_protocols,
			wm_delete_window: wm_delete_window,
		})
	}

	pub fn get_visual_info(xlib: &xlib::Xlib, display: *mut xlib::Display, screen_num: i32) -> xlib::XVisualInfo {
		let mut visualinfo: xlib::XVisualInfo = unsafe { zeroed() };
		unsafe {
			(xlib.XMatchVisualInfo)(display, screen_num, 32, xlib::TrueColor, &mut visualinfo);
		};
		return visualinfo;
	}

	pub fn map_window(&self) {
		unsafe{ (self.xlib.XMapWindow)(self.display, self.window) };
	}

	pub fn event_loop(&self) {
		// xlib event loop
		let mut event: xlib::XEvent = unsafe{ zeroed() };
		loop {
			unsafe { (self.xlib.XNextEvent)(self.display, &mut event) };
			match event.get_type() {
				xlib::ClientMessage => {
					let xclient: xlib::XClientMessageEvent = From::from(event);
					if xclient.message_type == self.wm_protocols && xclient.format == 32 {
						let protocol = xclient.data.get_long(0) as xlib::Atom;
						if protocol == self.wm_delete_window {
							break;
						}
					}
				},
				_ => {}
			}
		}
	}

}

impl Drop for XlibWindow {
	fn drop(&mut self) {
		println!("Dropping XlibWindow");
		unsafe {
			(self.xlib.XDestroyWindow)(self.display, self.window);
			(self.xlib.XCloseDisplay)(self.display);
		};
	}
}
