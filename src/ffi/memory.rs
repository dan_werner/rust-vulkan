#![allow(non_snake_case)]
#![allow(non_camel_case_types)]
#![allow(dead_code)]

extern crate libc;
use self::libc::uint32_t;
use self::libc::size_t;

use super::{
	VkFlags,
	VkStructureType,
	VkAccessFlags,
	VkOffset3D,
	VkExtent3D,
	VkDescriptorPool,
	VkDescriptorSetLayout,
};

use super::resource::{
	VkImage,
	VkImageLayout,
	VkImageSubresource,
	VkImageSubresourceRange,
	VkSparseImageFormatProperties,
	VkBuffer,
};

use super::device::{
	VkDeviceSize,
	VkDeviceMemory,
};


#[derive(Clone, Copy)]
#[repr(u32)]
pub enum VkMemoryPropertyFlagBits {
    VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT = 1,
    VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT = 2,
    VK_MEMORY_PROPERTY_HOST_COHERENT_BIT = 4,
    VK_MEMORY_PROPERTY_HOST_CACHED_BIT = 8,
    VK_MEMORY_PROPERTY_LAZILY_ALLOCATED_BIT = 16,
}

pub type VkMemoryPropertyFlags = VkFlags;

#[derive(Clone, Copy)]
#[repr(u32)]
pub enum VkMemoryHeapFlagBits { 
	VK_MEMORY_HEAP_DEVICE_LOCAL_BIT = 1,
	Dummy = 0,
}

pub type VkMemoryHeapFlags = VkFlags;
pub type VkMemoryMapFlags = VkFlags;


#[derive(Clone, Copy)]
#[repr(u32)]
pub enum VkSparseMemoryBindFlagBits {
    VK_SPARSE_MEMORY_BIND_METADATA_BIT = 1,
     Dummy = 0,
}

pub type VkSparseMemoryBindFlags = VkFlags;

#[repr(C)]
#[derive(Copy)]
pub struct VkMemoryBarrier {
    pub sType: VkStructureType,
    pub pNext: *const ::std::os::raw::c_void,
    pub srcAccessMask: VkAccessFlags,
    pub dstAccessMask: VkAccessFlags,
}
impl Clone for VkMemoryBarrier {
    fn clone(&self) -> Self { *self }
}
impl Default for VkMemoryBarrier {
    fn default() -> Self { unsafe { ::std::mem::zeroed() } }
}

#[repr(C)]
#[derive(Copy)]
pub struct VkImageMemoryBarrier {
    pub sType: VkStructureType,
    pub pNext: *const ::std::os::raw::c_void,
    pub srcAccessMask: VkAccessFlags,
    pub dstAccessMask: VkAccessFlags,
    pub oldLayout: VkImageLayout,
    pub newLayout: VkImageLayout,
    pub srcQueueFamilyIndex: uint32_t,
    pub dstQueueFamilyIndex: uint32_t,
    pub image: VkImage,
    pub subresourceRange: VkImageSubresourceRange,
}
impl Clone for VkImageMemoryBarrier {
    fn clone(&self) -> Self { *self }
}
impl Default for VkImageMemoryBarrier {
    fn default() -> Self { unsafe { ::std::mem::zeroed() } }
}

#[repr(C)]
#[derive(Copy)]
pub struct VkSparseImageMemoryRequirements {
    pub formatProperties: VkSparseImageFormatProperties,
    pub imageMipTailFirstLod: uint32_t,
    pub imageMipTailSize: VkDeviceSize,
    pub imageMipTailOffset: VkDeviceSize,
    pub imageMipTailStride: VkDeviceSize,
}
impl Clone for VkSparseImageMemoryRequirements {
    fn clone(&self) -> Self { *self }
}
impl Default for VkSparseImageMemoryRequirements {
    fn default() -> Self { unsafe { ::std::mem::zeroed() } }
}

#[repr(C)]
#[derive(Copy)]
pub struct VkBufferMemoryBarrier {
    pub sType: VkStructureType,
    pub pNext: *const ::std::os::raw::c_void,
    pub srcAccessMask: VkAccessFlags,
    pub dstAccessMask: VkAccessFlags,
    pub srcQueueFamilyIndex: uint32_t,
    pub dstQueueFamilyIndex: uint32_t,
    pub buffer: VkBuffer,
    pub offset: VkDeviceSize,
    pub size: VkDeviceSize,
}
impl Clone for VkBufferMemoryBarrier {
    fn clone(&self) -> Self { *self }
}
impl Default for VkBufferMemoryBarrier {
    fn default() -> Self { unsafe { ::std::mem::zeroed() } }
}

#[repr(C)]
#[derive(Copy)]
pub struct VkMemoryAllocateInfo {
    pub sType: VkStructureType,
    pub pNext: *const ::std::os::raw::c_void,
    pub allocationSize: VkDeviceSize,
    pub memoryTypeIndex: uint32_t,
}
impl Clone for VkMemoryAllocateInfo {
    fn clone(&self) -> Self { *self }
}
impl Default for VkMemoryAllocateInfo {
    fn default() -> Self { unsafe { ::std::mem::zeroed() } }
}

#[repr(C)]
#[derive(Copy)]
pub struct VkMappedMemoryRange {
    pub sType: VkStructureType,
    pub pNext: *const ::std::os::raw::c_void,
    pub memory: VkDeviceMemory,
    pub offset: VkDeviceSize,
    pub size: VkDeviceSize,
}
impl Clone for VkMappedMemoryRange {
    fn clone(&self) -> Self { *self }
}
impl Default for VkMappedMemoryRange {
    fn default() -> Self { unsafe { ::std::mem::zeroed() } }
}

#[repr(C)]
#[derive(Copy)]
pub struct VkMemoryRequirements {
    pub size: VkDeviceSize,
    pub alignment: VkDeviceSize,
    pub memoryTypeBits: uint32_t,
}
impl Clone for VkMemoryRequirements {
    fn clone(&self) -> Self { *self }
}
impl Default for VkMemoryRequirements {
    fn default() -> Self { unsafe { ::std::mem::zeroed() } }
}

#[repr(C)]
#[derive(Copy)]
pub struct VkSparseMemoryBind {
    pub resourceOffset: VkDeviceSize,
    pub size: VkDeviceSize,
    pub memory: VkDeviceMemory,
    pub memoryOffset: VkDeviceSize,
    pub flags: VkSparseMemoryBindFlags,
}
impl Clone for VkSparseMemoryBind {
    fn clone(&self) -> Self { *self }
}
impl Default for VkSparseMemoryBind {
    fn default() -> Self { unsafe { ::std::mem::zeroed() } }
}

#[repr(C)]
#[derive(Copy)]
pub struct VkSparseBufferMemoryBindInfo {
    pub buffer: VkBuffer,
    pub bindCount: uint32_t,
    pub pBinds: *const VkSparseMemoryBind,
}
impl Clone for VkSparseBufferMemoryBindInfo {
    fn clone(&self) -> Self { *self }
}
impl Default for VkSparseBufferMemoryBindInfo {
    fn default() -> Self { unsafe { ::std::mem::zeroed() } }
}

#[repr(C)]
#[derive(Copy)]
pub struct VkSparseImageOpaqueMemoryBindInfo {
    pub image: VkImage,
    pub bindCount: uint32_t,
    pub pBinds: *const VkSparseMemoryBind,
}
impl Clone for VkSparseImageOpaqueMemoryBindInfo {
    fn clone(&self) -> Self { *self }
}
impl Default for VkSparseImageOpaqueMemoryBindInfo {
    fn default() -> Self { unsafe { ::std::mem::zeroed() } }
}

#[repr(C)]
#[derive(Copy)]
pub struct VkMemoryType {
    pub propertyFlags: VkMemoryPropertyFlags,
    pub heapIndex: uint32_t,
}
impl Clone for VkMemoryType {
    fn clone(&self) -> Self { *self }
}
impl Default for VkMemoryType {
    fn default() -> Self { unsafe { ::std::mem::zeroed() } }
}

#[repr(C)]
#[derive(Copy)]
pub struct VkMemoryHeap {
    pub size: VkDeviceSize,
    pub flags: VkMemoryHeapFlags,
}
impl Clone for VkMemoryHeap {
    fn clone(&self) -> Self { *self }
}
impl Default for VkMemoryHeap {
    fn default() -> Self { unsafe { ::std::mem::zeroed() } }
}

#[repr(C)]
#[derive(Copy)]
pub struct VkAllocationCallbacks {
    pub pUserData: *mut ::std::os::raw::c_void,
    pub pfnAllocation: PFN_vkAllocationFunction,
    pub pfnReallocation: PFN_vkReallocationFunction,
    pub pfnFree: PFN_vkFreeFunction,
    pub pfnInternalAllocation: PFN_vkInternalAllocationNotification,
    pub pfnInternalFree: PFN_vkInternalFreeNotification,
}
impl Clone for VkAllocationCallbacks {
    fn clone(&self) -> Self { *self }
}
impl Default for VkAllocationCallbacks {
    fn default() -> Self { unsafe { ::std::mem::zeroed() } }
}

#[repr(C)]
#[derive(Copy)]
pub struct VkSparseImageMemoryBind {
    pub subresource: VkImageSubresource,
    pub offset: VkOffset3D,
    pub extent: VkExtent3D,
    pub memory: VkDeviceMemory,
    pub memoryOffset: VkDeviceSize,
    pub flags: VkSparseMemoryBindFlags,
}
impl Clone for VkSparseImageMemoryBind {
    fn clone(&self) -> Self { *self }
}
impl Default for VkSparseImageMemoryBind {
    fn default() -> Self { unsafe { ::std::mem::zeroed() } }
}

#[repr(C)]
#[derive(Copy)]
pub struct VkSparseImageMemoryBindInfo {
    pub image: VkImage,
    pub bindCount: uint32_t,
    pub pBinds: *const VkSparseImageMemoryBind,
}
impl Clone for VkSparseImageMemoryBindInfo {
    fn clone(&self) -> Self { *self }
}
impl Default for VkSparseImageMemoryBindInfo {
    fn default() -> Self { unsafe { ::std::mem::zeroed() } }
}


#[repr(C)]
#[derive(Copy)]
pub struct VkDescriptorSetAllocateInfo {
    pub sType: VkStructureType,
    pub pNext: *const ::std::os::raw::c_void,
    pub descriptorPool: VkDescriptorPool,
    pub descriptorSetCount: uint32_t,
    pub pSetLayouts: *const VkDescriptorSetLayout,
}
impl Clone for VkDescriptorSetAllocateInfo {
    fn clone(&self) -> Self { *self }
}
impl Default for VkDescriptorSetAllocateInfo {
    fn default() -> Self { unsafe { ::std::mem::zeroed() } }
}

pub type PFN_vkAllocationFunction = Option<unsafe extern "C" fn(
	pUserData: *mut ::std::os::raw::c_void,
	size: size_t,
	alignment: size_t,
	allocationScope: VkSystemAllocationScope
) -> *mut ::std::os::raw::c_void>;

pub type PFN_vkReallocationFunction = Option<unsafe extern "C" fn(
	pUserData: *mut ::std::os::raw::c_void,
	pOriginal: *mut ::std::os::raw::c_void,
	size: size_t,
	alignment: size_t,
	allocationScope: VkSystemAllocationScope
) -> *mut ::std::os::raw::c_void>;

pub type PFN_vkFreeFunction = Option<unsafe extern "C" fn(
	pUserData: *mut ::std::os::raw::c_void,
	pMemory: *mut ::std::os::raw::c_void
)>;

pub type PFN_vkInternalAllocationNotification = Option<unsafe extern "C" fn(
	pUserData: *mut ::std::os::raw::c_void,
	size: size_t,
	allocationType:
	VkInternalAllocationType,
	allocationScope: VkSystemAllocationScope
)>;

pub type PFN_vkInternalFreeNotification = Option<unsafe extern "C" fn(
	pUserData: *mut ::std::os::raw::c_void,
	size: size_t,
	allocationType: VkInternalAllocationType,
	allocationScope: VkSystemAllocationScope
)>;


pub const VK_SYSTEM_ALLOCATION_SCOPE_BEGIN_RANGE: VkSystemAllocationScope = VkSystemAllocationScope::VK_SYSTEM_ALLOCATION_SCOPE_COMMAND;
pub const VK_SYSTEM_ALLOCATION_SCOPE_END_RANGE: VkSystemAllocationScope = VkSystemAllocationScope::VK_SYSTEM_ALLOCATION_SCOPE_INSTANCE;

#[derive(Clone, Copy)]
#[repr(u32)]
pub enum VkSystemAllocationScope {
    VK_SYSTEM_ALLOCATION_SCOPE_COMMAND = 0,
    VK_SYSTEM_ALLOCATION_SCOPE_OBJECT = 1,
    VK_SYSTEM_ALLOCATION_SCOPE_CACHE = 2,
    VK_SYSTEM_ALLOCATION_SCOPE_DEVICE = 3,
    VK_SYSTEM_ALLOCATION_SCOPE_INSTANCE = 4,
    VK_SYSTEM_ALLOCATION_SCOPE_RANGE_SIZE = 5,
    VK_SYSTEM_ALLOCATION_SCOPE_MAX_ENUM = 2147483647,
}

pub const VK_INTERNAL_ALLOCATION_TYPE_BEGIN_RANGE: VkInternalAllocationType = VkInternalAllocationType::VK_INTERNAL_ALLOCATION_TYPE_EXECUTABLE;

pub const VK_INTERNAL_ALLOCATION_TYPE_END_RANGE: VkInternalAllocationType = VkInternalAllocationType::VK_INTERNAL_ALLOCATION_TYPE_EXECUTABLE;

#[derive(Clone, Copy)]
#[repr(u32)]
pub enum VkInternalAllocationType {
    VK_INTERNAL_ALLOCATION_TYPE_EXECUTABLE = 0,
    VK_INTERNAL_ALLOCATION_TYPE_RANGE_SIZE = 1,
    VK_INTERNAL_ALLOCATION_TYPE_MAX_ENUM = 2147483647,
}
