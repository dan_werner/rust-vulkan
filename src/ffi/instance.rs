#![allow(non_snake_case)]
#![allow(non_camel_case_types)]
#![allow(dead_code)]

extern crate libc;

use self::libc::{
	uint32_t,
	uint64_t,
	int32_t,
	size_t,
};


use super::{
	VkStructureType,
	VkApplicationInfo,
	VkFlags,
	VkResult,
	PFN_vkVoidFunction,
	VkSurfaceKHR,
	VkDisplaySurfaceCreateInfoKHR,
	VkDebugReportCallbackCreateInfoEXT,
	VkDebugReportCallbackEXT,
	VkDebugReportObjectTypeEXT,
	VkDebugReportFlagsEXT,
};

use super::memory::{
	VkAllocationCallbacks,
};

use super::physical_device::{
	VkPhysicalDevice
};

use super::khr::{
	VkXlibSurfaceCreateInfoKHR,
};


pub type VkInstanceCreateFlags = VkFlags;
pub enum Struct_VkInstance_T { }
pub type VkInstance = *mut Struct_VkInstance_T;

#[repr(C)]
#[derive(Copy)]
pub struct VkInstanceCreateInfo {
    pub sType: VkStructureType,
    pub pNext: *const ::std::os::raw::c_void,
    pub flags: VkInstanceCreateFlags,
    pub pApplicationInfo: *const VkApplicationInfo,
    pub enabledLayerCount: uint32_t,
    pub ppEnabledLayerNames: *const *const ::std::os::raw::c_char,
    pub enabledExtensionCount: uint32_t,
    pub ppEnabledExtensionNames: *const *const ::std::os::raw::c_char,
}
impl Clone for VkInstanceCreateInfo {
    fn clone(&self) -> Self { *self }
}
impl Default for VkInstanceCreateInfo {
    fn default() -> Self { unsafe { ::std::mem::zeroed() } }
}

pub type PFN_vkGetInstanceProcAddr = Option<unsafe extern "C" fn(
	instance: VkInstance,
	pName: *const ::std::os::raw::c_char
) -> PFN_vkVoidFunction>;

pub type PFN_vkCreateInstance = Option<unsafe extern "C" fn(
	pCreateInfo: *const VkInstanceCreateInfo,
	pAllocator: *const VkAllocationCallbacks,
	pInstance: *mut VkInstance
) -> VkResult>;

pub type PFN_vkDestroyInstance = Option<unsafe extern "C" fn(
	instance: VkInstance,
	pAllocator: *const VkAllocationCallbacks
)>;

pub type PFN_vkEnumeratePhysicalDevices = Option<unsafe extern "C" fn(
	instance: VkInstance,
	pPhysicalDeviceCount: *mut uint32_t,
	pPhysicalDevices: *mut VkPhysicalDevice
) -> VkResult>;

pub type PFN_vkDestroySurfaceKHR = Option<unsafe extern "C" fn(
	instance: VkInstance,
	surface: VkSurfaceKHR,
	pAllocator: *const VkAllocationCallbacks
)>;

pub type PFN_vkCreateDisplayPlaneSurfaceKHR = Option<unsafe extern "C" fn(
	instance: VkInstance,
	pCreateInfo: *const VkDisplaySurfaceCreateInfoKHR,
	pAllocator: *const VkAllocationCallbacks,
	pSurface: *mut VkSurfaceKHR
) -> VkResult>;

pub type PFN_vkCreateDebugReportCallbackEXT = Option<unsafe extern "C" fn(
	instance: VkInstance,
	pCreateInfo: *const VkDebugReportCallbackCreateInfoEXT,
	pAllocator: *const VkAllocationCallbacks,
	pCallback: *mut VkDebugReportCallbackEXT
) -> VkResult>;

pub type PFN_vkDestroyDebugReportCallbackEXT = Option<unsafe extern "C" fn(
	instance: VkInstance,
	callback: VkDebugReportCallbackEXT,
	pAllocator: *const VkAllocationCallbacks
)>;

pub type PFN_vkDebugReportMessageEXT = Option<unsafe extern "C" fn(
	instance: VkInstance,
	flags: VkDebugReportFlagsEXT,
	objectType: VkDebugReportObjectTypeEXT,
	object: uint64_t,
	location: size_t,
	messageCode: int32_t,
	pLayerPrefix: *const ::std::os::raw::c_char,
	pMessage: *const ::std::os::raw::c_char
)>;

extern "C" {
    pub fn vkCreateInstance(
			pCreateInfo: *const VkInstanceCreateInfo,
			pAllocator: *const VkAllocationCallbacks,
			pInstance: *mut VkInstance
		) -> VkResult;

    pub fn vkDestroyInstance(
			instance: VkInstance,
			pAllocator: *const VkAllocationCallbacks
		);

    pub fn vkEnumeratePhysicalDevices(
			instance: VkInstance,
			pPhysicalDeviceCount: *mut uint32_t,
			pPhysicalDevices: *mut VkPhysicalDevice
		) -> VkResult;
		
		pub fn vkGetInstanceProcAddr(
			instance: VkInstance,
			pName: *const ::std::os::raw::c_char
		) -> PFN_vkVoidFunction;

		pub fn vkDestroySurfaceKHR(
			instance: VkInstance, surface: VkSurfaceKHR,
			pAllocator: *const VkAllocationCallbacks
		);

    pub fn vkCreateDisplayPlaneSurfaceKHR(
			instance: VkInstance,
			pCreateInfo: *const VkDisplaySurfaceCreateInfoKHR,
			pAllocator: *const VkAllocationCallbacks,
			pSurface: *mut VkSurfaceKHR
		) -> VkResult;

    pub fn vkCreateDebugReportCallbackEXT(
			instance: VkInstance,
			pCreateInfo: *const VkDebugReportCallbackCreateInfoEXT,
			pAllocator: *const VkAllocationCallbacks,
			pCallback: *mut VkDebugReportCallbackEXT
		) -> VkResult;

    pub fn vkDestroyDebugReportCallbackEXT(
			instance: VkInstance,
			callback: VkDebugReportCallbackEXT,
			pAllocator: *const VkAllocationCallbacks
		);
		
    pub fn vkDebugReportMessageEXT(
			instance: VkInstance,
			flags: VkDebugReportFlagsEXT,
			objectType: VkDebugReportObjectTypeEXT,
			object: uint64_t,
			location: size_t,
			messageCode: int32_t,
			pLayerPrefix: *const ::std::os::raw::c_char,
			pMessage: *const ::std::os::raw::c_char
		);

    pub fn vkCreateXlibSurfaceKHR(
			instance: VkInstance,
			pCreateInfo: *const VkXlibSurfaceCreateInfoKHR,
			pAllocator: *const VkAllocationCallbacks,
			pSurface: *mut VkSurfaceKHR
		) -> VkResult;
}
