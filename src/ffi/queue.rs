#![allow(non_snake_case)]
#![allow(non_camel_case_types)]
#![allow(dead_code)]

extern crate libc;
use self::libc::uint32_t;

use super::{
	VkResult,
	VkFlags,

	VkExtent3D,

	VkSubmitInfo,

	VkFence,
	VkBindSparseInfo,
	VkPresentInfoKHR,

};


pub enum Struct_VkQueue_T { }
pub type VkQueue = *mut Struct_VkQueue_T;
pub type VkQueueFlags = VkFlags;

#[derive(Clone, Copy)]
#[repr(u32)]
pub enum VkQueueFlagBits {
    VK_QUEUE_GRAPHICS_BIT = 1,
    VK_QUEUE_COMPUTE_BIT = 2,
    VK_QUEUE_TRANSFER_BIT = 4,
    VK_QUEUE_SPARSE_BINDING_BIT = 8,
}


#[repr(C)]
#[derive(Copy)]
pub struct VkQueueFamilyProperties {
    pub queueFlags: VkQueueFlags,
    pub queueCount: uint32_t,
    pub timestampValidBits: uint32_t,
    pub minImageTransferGranularity: VkExtent3D,
}
impl Clone for VkQueueFamilyProperties {
    fn clone(&self) -> Self { *self }
}
impl Default for VkQueueFamilyProperties {
    fn default() -> Self { unsafe { ::std::mem::zeroed() } }
}


pub type PFN_vkQueueSubmit = Option<unsafe extern "C" fn(
	queue: VkQueue,
	submitCount: uint32_t,
	pSubmits: *const VkSubmitInfo,
	fence: VkFence
) -> VkResult>;

pub type PFN_vkQueueBindSparse = Option<unsafe extern "C" fn(
	queue: VkQueue,
	bindInfoCount: uint32_t,
	pBindInfo: *const VkBindSparseInfo,
	fence: VkFence
) -> VkResult>;

pub type PFN_vkQueueWaitIdle = Option<extern "C" fn(queue: VkQueue) -> VkResult>;

pub type PFN_vkQueuePresentKHR = Option<unsafe extern "C" fn(
	queue: VkQueue,
	pPresentInfo: *const VkPresentInfoKHR
) -> VkResult>;

extern "C" {

		pub fn vkQueueSubmit(
			queue: VkQueue,
			submitCount: uint32_t,
			pSubmits: *const VkSubmitInfo,
			fence: VkFence
		) -> VkResult;

    pub fn vkQueueWaitIdle(queue: VkQueue) -> VkResult;

    pub fn vkQueueBindSparse(
			queue: VkQueue,
			bindInfoCount: uint32_t,
      pBindInfo: *const VkBindSparseInfo,
      fence: VkFence
		) -> VkResult;

		pub fn vkQueuePresentKHR(
			queue: VkQueue,
			pPresentInfo: *const VkPresentInfoKHR
		) -> VkResult;
}
