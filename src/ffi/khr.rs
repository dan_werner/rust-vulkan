extern crate x11_dl;

use self::x11_dl::xlib::{
	Window,
	Display,
	VisualID,
};

use libc::uint32_t;

use std::clone::Clone;
use std::default::Default;

use super::{
	VkBool32,
	VkStructureType,
	VkResult,
	VkFlags,
	VkSurfaceKHR,
};

use super::instance::{
	VkInstance,
};

use super::memory::{
	VkAllocationCallbacks,
};

use super::physical_device::VkPhysicalDevice;

pub type VkXlibSurfaceCreateFlagsKHR = VkFlags;
#[repr(C)]
#[derive(Copy)]
pub struct VkXlibSurfaceCreateInfoKHR {
    pub sType: VkStructureType,
    pub pNext: *const ::std::os::raw::c_void,
    pub flags: VkXlibSurfaceCreateFlagsKHR,
    pub dpy: *mut Display,
    pub window: Window,
}
impl Clone for VkXlibSurfaceCreateInfoKHR {
    fn clone(&self) -> Self { *self }
}
impl Default for VkXlibSurfaceCreateInfoKHR {
    fn default() -> Self { unsafe { ::std::mem::zeroed() } }
}

pub type PFN_vkCreateXlibSurfaceKHR = Option<unsafe extern "C" fn(
	instance: VkInstance,
	pCreateInfo: *const VkXlibSurfaceCreateInfoKHR,
	pAllocator: *const VkAllocationCallbacks,
	pSurface: *mut VkSurfaceKHR
) -> VkResult>;

pub type PFN_vkGetPhysicalDeviceXlibPresentationSupportKHR = Option<unsafe extern "C" fn(
	physicalDevice:
	VkPhysicalDevice,
	queueFamilyIndex: uint32_t,
	dpy: *mut Display,
	visualID: VisualID
) -> VkBool32>;

/*unresolved import `self::xcb::xcb_window_t`. There is no `xcb_window_t` in `ffi::khr::xcb` [E0432]
pub type VkXcbSurfaceCreateFlagsKHR = VkFlags;


#[repr(C)]
#[derive(Copy)]
pub struct VkXcbSurfaceCreateInfoKHR {
    pub sType: VkStructureType,
    pub pNext: *const ::std::os::raw::c_void,
    pub flags: VkXcbSurfaceCreateFlagsKHR,
    pub connection: *mut xcb_connection_t,
    pub window: xcb_window_t,
}
impl Clone for VkXcbSurfaceCreateInfoKHR {
    fn clone(&self) -> Self { *self }
}
impl ::std::default::Default for VkXcbSurfaceCreateInfoKHR {
    fn default() -> Self { unsafe { ::std::mem::zeroed() } }
}

pub type PFN_vkCreateXcbSurfaceKHR = Option<unsafe extern "C" fn(
	instance: VkInstance,
	pCreateInfo: *const VkXcbSurfaceCreateInfoKHR,
	pAllocator: *const VkAllocationCallbacks,
	pSurface: *mut VkSurfaceKHR
) -> VkResult>;
pub type PFN_vkGetPhysicalDeviceXcbPresentationSupportKHR = Option<unsafe extern "C" fn(
	physicalDevice: VkPhysicalDevice,
	queueFamilyIndex: uint32_t,
	connection: *mut xcb_connection_t,
	visual_id: xcb_visualid_t
) -> VkBool32>;
*/
