#[cfg(target_os="windows")]
#[link(name = "vulkan-1")]
extern {}

#[cfg(any(target_os="linux", target_os="freebsd", target_os="dragonfly"))]
#[link(name = "vulkan")]
extern {}
