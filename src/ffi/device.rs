#![allow(non_snake_case)]
#![allow(non_camel_case_types)]
#![allow(dead_code)]

extern crate libc;
use self::libc::uint32_t;
use self::libc::uint64_t;
use self::libc::size_t;

use super::{
	VkFlags,
	VkResult,
	VkBool32,
	VkStructureType,
	PFN_vkVoidFunction,

	VkFenceCreateInfo,
	VkFence,

	VkExtent2D,


	VkSwapchainCreateInfoKHR,
	VkSwapchainKHR,

	VkSemaphoreCreateInfo,
	VkSemaphore,

	VkEvent,
	VkEventCreateInfo,

	VkQueryPool,
	VkQueryPoolCreateInfo,
	VkQueryResultFlags,


	VkShaderModule,
	VkShaderModuleCreateInfo,

	
	VkPipelineCacheCreateInfo,
	VkPipelineCache,
	VkGraphicsPipelineCreateInfo,

	VkPipeline,
	VkPipelineLayout,
	VkPipelineLayoutCreateInfo,

	VkSampler,
	VkSamplerCreateInfo,


	VkComputePipelineCreateInfo,

	VkDescriptorPool,
	VkDescriptorPoolCreateInfo,
	VkDescriptorPoolResetFlags,

	VkDescriptorSet,
	VkDescriptorSetLayout,
	VkDescriptorSetLayoutCreateInfo,
	VkWriteDescriptorSet,
	VkCopyDescriptorSet,

	VkFramebuffer,
	VkFramebufferCreateInfo,
	VkRenderPass,
	VkRenderPassCreateInfo,
};

use super::queue::{
	VkQueue,
};

use super::physical_device::{
	VkPhysicalDeviceFeatures,
};

use super::command_buffer::{
	VkCommandBufferAllocateInfo,
	VkCommandBuffer,
	VkCommandPool,
	VkCommandPoolCreateInfo,
	VkCommandPoolResetFlags,
};

use super::resource::{
	VkImage,
	VkImageView,
	VkImageCreateInfo,
	VkImageViewCreateInfo,
	VkImageSubresource,
	VkSubresourceLayout,

	VkBuffer,
	VkBufferView,
	VkBufferViewCreateInfo,
	VkBufferCreateInfo,

};

use super::memory::{
	VkMemoryMapFlags,
	VkSparseImageMemoryRequirements,
	VkMemoryAllocateInfo,
	VkMappedMemoryRange,
	VkMemoryRequirements,
	VkAllocationCallbacks,
	VkDescriptorSetAllocateInfo,
};

pub type VkDeviceSize = uint64_t;
pub type VkDeviceCreateFlags = VkFlags;

pub enum Struct_VkDeviceMemory_T { }
pub type VkDeviceMemory = *mut Struct_VkDeviceMemory_T;

pub enum Struct_VkDevice_T { }
pub type VkDevice = *mut Struct_VkDevice_T;

pub type VkDeviceQueueCreateFlags = VkFlags;

#[repr(C)]
#[derive(Copy)]
pub struct VkDeviceCreateInfo {
    pub sType: VkStructureType,
    pub pNext: *const ::std::os::raw::c_void,
    pub flags: VkDeviceCreateFlags,
    pub queueCreateInfoCount: uint32_t,
    pub pQueueCreateInfos: *const VkDeviceQueueCreateInfo,
    pub enabledLayerCount: uint32_t,
    pub ppEnabledLayerNames: *const *const ::std::os::raw::c_char,
    pub enabledExtensionCount: uint32_t,
    pub ppEnabledExtensionNames: *const *const ::std::os::raw::c_char,
    pub pEnabledFeatures: *const VkPhysicalDeviceFeatures,
}
impl Clone for VkDeviceCreateInfo {
    fn clone(&self) -> Self { *self }
}
impl Default for VkDeviceCreateInfo {
    fn default() -> Self { unsafe { ::std::mem::zeroed() } }
}

#[repr(C)]
#[derive(Copy)]
pub struct VkDeviceQueueCreateInfo {
    pub sType: VkStructureType,
    pub pNext: *const ::std::os::raw::c_void,
    pub flags: VkDeviceQueueCreateFlags,
    pub queueFamilyIndex: uint32_t,
    pub queueCount: uint32_t,
    pub pQueuePriorities: *const ::std::os::raw::c_float,
}
impl Clone for VkDeviceQueueCreateInfo {
    fn clone(&self) -> Self { *self }
}
impl Default for VkDeviceQueueCreateInfo {
    fn default() -> Self { unsafe { ::std::mem::zeroed() } }
}

pub type PFN_vkGetDeviceQueue = Option<unsafe extern "C" fn(
	device: VkDevice,
	queueFamilyIndex: uint32_t,
	queueIndex: uint32_t,
	pQueue: *mut VkQueue
)>;

pub type PFN_vkGetDeviceProcAddr = Option<unsafe extern "C" fn(
	device: VkDevice,
	pName: *const ::std::os::raw::c_char
) -> PFN_vkVoidFunction>;

pub type PFN_vkDeviceWaitIdle = Option<extern "C" fn(device: VkDevice) -> VkResult>;

pub type PFN_vkAllocateMemory = Option<unsafe extern "C" fn(
	device: VkDevice,
	pAllocateInfo: *const VkMemoryAllocateInfo,
	pAllocator: *const VkAllocationCallbacks,
	pMemory: *mut VkDeviceMemory
) -> VkResult>;

pub type PFN_vkFreeMemory = Option<unsafe extern "C" fn(
	device: VkDevice,
	memory: VkDeviceMemory,
	pAllocator: *const VkAllocationCallbacks
)>;

pub type PFN_vkMapMemory = Option<unsafe extern "C" fn(
	device: VkDevice,
	memory: VkDeviceMemory,
	offset: VkDeviceSize,
	size: VkDeviceSize,
	flags: VkMemoryMapFlags,
	ppData: *mut *mut ::std::os::raw::c_void
) -> VkResult>;

pub type PFN_vkUnmapMemory = Option<extern "C" fn(device: VkDevice, memory: VkDeviceMemory)>;

pub type PFN_vkFlushMappedMemoryRanges = Option<unsafe extern "C" fn(
	device: VkDevice,
	memoryRangeCount: uint32_t,
	pMemoryRanges: *const VkMappedMemoryRange
) -> VkResult>;

pub type PFN_vkInvalidateMappedMemoryRanges = Option<unsafe extern "C" fn(
	device: VkDevice,
	memoryRangeCount: uint32_t,
	pMemoryRanges: *const VkMappedMemoryRange
) -> VkResult>;

pub type PFN_vkGetDeviceMemoryCommitment = Option<unsafe extern "C" fn(
	device: VkDevice,
	memory: VkDeviceMemory,
	pCommittedMemoryInBytes: *mut VkDeviceSize
)>;

pub type PFN_vkBindBufferMemory = Option<extern "C" fn(
	device: VkDevice,
	buffer: VkBuffer,
	memory: VkDeviceMemory,
	memoryOffset: VkDeviceSize
) -> VkResult>;

pub type PFN_vkBindImageMemory = Option<extern "C" fn(
	device: VkDevice,
	image: VkImage,
	memory: VkDeviceMemory,
	memoryOffset: VkDeviceSize
) -> VkResult>;

pub type PFN_vkGetBufferMemoryRequirements = Option<unsafe extern "C" fn(
	device: VkDevice,
	buffer: VkBuffer,
	pMemoryRequirements: *mut VkMemoryRequirements
)>;

pub type PFN_vkGetImageMemoryRequirements = Option<unsafe extern "C" fn(
	device: VkDevice,
	image: VkImage,
	pMemoryRequirements: *mut VkMemoryRequirements
)>;

pub type PFN_vkGetImageSparseMemoryRequirements = Option<unsafe extern "C" fn(
	device: VkDevice,
	image: VkImage,
	pSparseMemoryRequirementCount: *mut uint32_t,
	pSparseMemoryRequirements: *mut VkSparseImageMemoryRequirements
)>;

pub type PFN_vkCreateFence = Option<unsafe extern "C" fn(
	device: VkDevice,
	pCreateInfo: *const VkFenceCreateInfo,
	pAllocator: *const VkAllocationCallbacks,
	pFence: *mut VkFence
) -> VkResult>;

pub type PFN_vkDestroyFence = Option<unsafe extern "C" fn(
	device: VkDevice,
	fence: VkFence,
	pAllocator: *const VkAllocationCallbacks
)>;

pub type PFN_vkResetFences = Option<unsafe extern "C" fn(
	device: VkDevice,
	fenceCount: uint32_t,
	pFences: *const VkFence
) -> VkResult>;

pub type PFN_vkGetFenceStatus = Option<extern "C" fn(device: VkDevice, fence: VkFence) -> VkResult>;

pub type PFN_vkWaitForFences = Option<unsafe extern "C" fn(
	device: VkDevice,
	fenceCount: uint32_t,
	pFences: *const VkFence,
	waitAll: VkBool32,
	timeout: uint64_t
) -> VkResult>;

pub type PFN_vkCreateSemaphore = Option<unsafe extern "C" fn(
	device: VkDevice,
	pCreateInfo: *const VkSemaphoreCreateInfo,
	pAllocator: *const VkAllocationCallbacks,
	pSemaphore: *mut VkSemaphore
) -> VkResult>;

pub type PFN_vkDestroySemaphore = Option<unsafe extern "C" fn(
	device: VkDevice,
	semaphore: VkSemaphore,
	pAllocator: *const VkAllocationCallbacks
)>;

pub type PFN_vkCreateEvent = Option<unsafe extern "C" fn(
	device: VkDevice,
	pCreateInfo: *const VkEventCreateInfo,
	pAllocator: *const VkAllocationCallbacks,
	pEvent: *mut VkEvent
) -> VkResult>;

pub type PFN_vkDestroyEvent = Option<unsafe extern "C" fn(
	device: VkDevice,
	event: VkEvent,
	pAllocator: *const VkAllocationCallbacks
)>;

pub type PFN_vkGetEventStatus =
	Option<extern "C" fn( device: VkDevice, event: VkEvent) -> VkResult>;

pub type PFN_vkSetEvent = 
	Option<extern "C" fn( device: VkDevice, event: VkEvent) -> VkResult>;

pub type PFN_vkResetEvent =
	Option<extern "C" fn( device: VkDevice, event: VkEvent) -> VkResult>;

pub type PFN_vkCreateQueryPool = Option<unsafe extern "C" fn(
	device: VkDevice,
	pCreateInfo: *const VkQueryPoolCreateInfo,
	pAllocator: *const VkAllocationCallbacks,
	pQueryPool: *mut VkQueryPool
) -> VkResult>;

pub type PFN_vkDestroyQueryPool = Option<unsafe extern "C" fn(
	device: VkDevice,
	queryPool: VkQueryPool,
	pAllocator: *const VkAllocationCallbacks
)>;

pub type PFN_vkGetQueryPoolResults = Option<unsafe extern "C" fn(
	device: VkDevice,
	queryPool: VkQueryPool,
	firstQuery: uint32_t,
	queryCount: uint32_t,
	dataSize: size_t,
	pData: *mut ::std::os::raw::c_void,
	stride: VkDeviceSize,
	flags: VkQueryResultFlags
) -> VkResult>;

pub type PFN_vkCreateBuffer = Option<unsafe extern "C" fn(
	device: VkDevice,
	pCreateInfo: *const VkBufferCreateInfo,
	pAllocator: *const VkAllocationCallbacks,
	pBuffer: *mut VkBuffer
) -> VkResult>;

pub type PFN_vkDestroyBuffer = Option<unsafe extern "C" fn(
	device: VkDevice,
	buffer: VkBuffer,
	pAllocator: *const VkAllocationCallbacks
)>;

pub type PFN_vkCreateBufferView = Option<unsafe extern "C" fn(
	device: VkDevice,
	pCreateInfo: *const VkBufferViewCreateInfo,
	pAllocator: *const VkAllocationCallbacks,
	pView: *mut VkBufferView
) -> VkResult>;

pub type PFN_vkDestroyBufferView = Option<unsafe extern "C" fn(
	device: VkDevice,
	bufferView: VkBufferView,
	pAllocator: *const VkAllocationCallbacks
)>;

pub type PFN_vkCreateImage = Option<unsafe extern "C" fn(
	device: VkDevice,
	pCreateInfo: *const VkImageCreateInfo,
	pAllocator: *const VkAllocationCallbacks,
	pImage: *mut VkImage
) -> VkResult>;

pub type PFN_vkDestroyImage = Option<unsafe extern "C" fn(
	device: VkDevice,
	image: VkImage,
	pAllocator: *const VkAllocationCallbacks
)>;

pub type PFN_vkGetImageSubresourceLayout = Option<unsafe extern "C" fn(
	device: VkDevice,
	image: VkImage,
	pSubresource: *const VkImageSubresource,
	pLayout: *mut VkSubresourceLayout
)>;

pub type PFN_vkCreateImageView = Option<unsafe extern "C" fn(
	device: VkDevice,
	pCreateInfo: *const VkImageViewCreateInfo,
	pAllocator: *const VkAllocationCallbacks,
	pView: *mut VkImageView
) -> VkResult>;

pub type PFN_vkDestroyImageView = Option<unsafe extern "C" fn(
	device: VkDevice,
	imageView: VkImageView,
	pAllocator: *const VkAllocationCallbacks
)>;

pub type PFN_vkCreateShaderModule = Option<unsafe extern "C" fn(
	device: VkDevice,
	pCreateInfo: *const VkShaderModuleCreateInfo,
	pAllocator: *const VkAllocationCallbacks,
	pShaderModule: *mut VkShaderModule
) -> VkResult>;

pub type PFN_vkDestroyShaderModule = Option<unsafe extern "C" fn(
	device: VkDevice,
	shaderModule: VkShaderModule,
	pAllocator:
	*const VkAllocationCallbacks
)>;

pub type PFN_vkCreatePipelineCache = Option<unsafe extern "C" fn(
	device: VkDevice,
	pCreateInfo: *const VkPipelineCacheCreateInfo,
	pAllocator: *const VkAllocationCallbacks,
	pPipelineCache: *mut VkPipelineCache
) -> VkResult>;

pub type PFN_vkDestroyPipelineCache = Option<unsafe extern "C" fn(
	device: VkDevice,
	pipelineCache: VkPipelineCache,
	pAllocator: *const VkAllocationCallbacks
)>;

pub type PFN_vkGetPipelineCacheData = Option<unsafe extern "C" fn(
	device: VkDevice,
	pipelineCache: VkPipelineCache,
	pDataSize: *mut size_t,
	pData: *mut ::std::os::raw::c_void
) -> VkResult>;

pub type PFN_vkMergePipelineCaches = Option<unsafe extern "C" fn(
	device: VkDevice,
	dstCache: VkPipelineCache,
	srcCacheCount: uint32_t,
	pSrcCaches: *const VkPipelineCache
) -> VkResult>;

pub type PFN_vkCreateGraphicsPipelines = Option<unsafe extern "C" fn(
	device: VkDevice,
	pipelineCache: VkPipelineCache,
	createInfoCount: uint32_t,
	pCreateInfos: *const VkGraphicsPipelineCreateInfo,
	pAllocator: *const VkAllocationCallbacks,
	pPipelines: *mut VkPipeline
) -> VkResult>;

pub type PFN_vkCreateComputePipelines = Option<unsafe extern "C" fn(
	device: VkDevice,
	pipelineCache: VkPipelineCache,
	createInfoCount: uint32_t,
	pCreateInfos: *const VkComputePipelineCreateInfo,
	pAllocator: *const VkAllocationCallbacks,
	pPipelines: *mut VkPipeline
) -> VkResult>;

pub type PFN_vkDestroyPipeline = Option<unsafe extern "C" fn(
	device: VkDevice,
	pipeline: VkPipeline,
	pAllocator: *const VkAllocationCallbacks
)>;

pub type PFN_vkCreatePipelineLayout = Option<unsafe extern "C" fn(
	device: VkDevice,
	pCreateInfo: *const VkPipelineLayoutCreateInfo,
	pAllocator: *const VkAllocationCallbacks,
	pPipelineLayout: *mut VkPipelineLayout
) -> VkResult>;

pub type PFN_vkDestroyPipelineLayout = Option<unsafe extern "C" fn(
	device: VkDevice,
	pipelineLayout: VkPipelineLayout,
	pAllocator: *const VkAllocationCallbacks
)>;

pub type PFN_vkCreateSampler = Option<unsafe extern "C" fn(
	device: VkDevice,
	pCreateInfo: *const VkSamplerCreateInfo,
	pAllocator: *const VkAllocationCallbacks,
	pSampler: *mut VkSampler
) -> VkResult>;

pub type PFN_vkDestroySampler = Option<unsafe extern "C" fn(
	device: VkDevice,
	sampler: VkSampler,
	pAllocator: *const VkAllocationCallbacks
)>;

pub type PFN_vkCreateDescriptorSetLayout = Option<unsafe extern "C" fn(
	device: VkDevice,
	pCreateInfo: *const VkDescriptorSetLayoutCreateInfo,
	pAllocator: *const VkAllocationCallbacks,
	pSetLayout: *mut VkDescriptorSetLayout
) -> VkResult>;

pub type PFN_vkDestroyDescriptorSetLayout = Option<unsafe extern "C" fn(
	device: VkDevice,
	descriptorSetLayout:
	VkDescriptorSetLayout,
	pAllocator: *const VkAllocationCallbacks
)>;

pub type PFN_vkCreateDescriptorPool = Option<unsafe extern "C" fn(
	device: VkDevice,
	pCreateInfo: *const VkDescriptorPoolCreateInfo,
	pAllocator: *const VkAllocationCallbacks,
	pDescriptorPool: *mut VkDescriptorPool
) -> VkResult>;

pub type PFN_vkDestroyDescriptorPool = Option<unsafe extern "C" fn(
	device: VkDevice,
	descriptorPool:
	VkDescriptorPool,
	pAllocator: *const VkAllocationCallbacks
)>;

pub type PFN_vkResetDescriptorPool = Option<extern "C" fn(
	device: VkDevice,
	descriptorPool: VkDescriptorPool,
	flags: VkDescriptorPoolResetFlags
) -> VkResult>;

pub type PFN_vkAllocateDescriptorSets = Option<unsafe extern "C" fn(
	device: VkDevice,
	pAllocateInfo: *const VkDescriptorSetAllocateInfo,
	pDescriptorSets: *mut VkDescriptorSet
) -> VkResult>;

pub type PFN_vkFreeDescriptorSets = Option<unsafe extern "C" fn(
	device: VkDevice,
	descriptorPool:
	VkDescriptorPool,
	descriptorSetCount: uint32_t,
	pDescriptorSets: *const VkDescriptorSet
) -> VkResult>;

pub type PFN_vkUpdateDescriptorSets = Option<unsafe extern "C" fn(
	device: VkDevice,
	descriptorWriteCount: uint32_t,
	pDescriptorWrites: *const VkWriteDescriptorSet,
	descriptorCopyCount: uint32_t,
	pDescriptorCopies: *const VkCopyDescriptorSet
)>;	

pub type PFN_vkCreateFramebuffer = Option<unsafe extern "C" fn(
	device: VkDevice,
	pCreateInfo: *const VkFramebufferCreateInfo,
	pAllocator: *const VkAllocationCallbacks,
	pFramebuffer: *mut VkFramebuffer
) -> VkResult>;

pub type PFN_vkDestroyFramebuffer = Option<unsafe extern "C" fn(
	device: VkDevice,
	framebuffer: VkFramebuffer,
	pAllocator: *const VkAllocationCallbacks
)>;

pub type PFN_vkCreateRenderPass = Option<unsafe extern "C" fn(
	device: VkDevice,
	pCreateInfo: *const VkRenderPassCreateInfo,
	pAllocator: *const VkAllocationCallbacks,
	pRenderPass: *mut VkRenderPass
) -> VkResult>;

pub type PFN_vkDestroyRenderPass = Option<unsafe extern "C" fn(
	device: VkDevice,
	renderPass: VkRenderPass,
	pAllocator: *const VkAllocationCallbacks
)>;

pub type PFN_vkGetRenderAreaGranularity = Option<unsafe extern "C" fn(
	device: VkDevice,
	renderPass: VkRenderPass,
	pGranularity: *mut VkExtent2D
)>;

pub type PFN_vkCreateCommandPool = Option<unsafe extern "C" fn(
	device: VkDevice,
	pCreateInfo: *const VkCommandPoolCreateInfo,
	pAllocator: *const VkAllocationCallbacks,
	pCommandPool: *mut VkCommandPool
) -> VkResult>;

pub type PFN_vkDestroyCommandPool = Option<unsafe extern "C" fn(
	device: VkDevice,
	commandPool: VkCommandPool,
	pAllocator: *const VkAllocationCallbacks
)>;

pub type PFN_vkResetCommandPool = Option<extern "C" fn(
	device: VkDevice,
	commandPool: VkCommandPool,
	flags: VkCommandPoolResetFlags
) -> VkResult>;

pub type PFN_vkAllocateCommandBuffers = Option<unsafe extern "C" fn(
	device: VkDevice,
	pAllocateInfo: *const VkCommandBufferAllocateInfo,
	pCommandBuffers: *mut VkCommandBuffer
) -> VkResult>;

pub type PFN_vkFreeCommandBuffers = Option<unsafe extern "C" fn(
	device: VkDevice,
	commandPool: VkCommandPool,
	commandBufferCount: uint32_t,
	pCommandBuffers: *const VkCommandBuffer
)>;

pub type PFN_vkCreateSwapchainKHR = Option<unsafe extern "C" fn(
	device: VkDevice,
	pCreateInfo: *const VkSwapchainCreateInfoKHR,
	pAllocator: *const VkAllocationCallbacks,
	pSwapchain: *mut VkSwapchainKHR
) -> VkResult>;

pub type PFN_vkDestroySwapchainKHR = Option<unsafe extern "C" fn(
	device: VkDevice,
	swapchain: VkSwapchainKHR,
	pAllocator:
	*const VkAllocationCallbacks
)>;

pub type PFN_vkGetSwapchainImagesKHR = Option<unsafe extern "C" fn(
	device: VkDevice,
	swapchain: VkSwapchainKHR,
	pSwapchainImageCount: *mut uint32_t,
	pSwapchainImages: *mut VkImage
) -> VkResult>;

pub type PFN_vkAcquireNextImageKHR = Option<unsafe extern "C" fn(
	device: VkDevice,
	swapchain: VkSwapchainKHR,
	timeout: uint64_t,
	semaphore: VkSemaphore,
	fence: VkFence,
	pImageIndex: *mut uint32_t
) -> VkResult>;

pub type PFN_vkCreateSharedSwapchainsKHR = Option<unsafe extern "C" fn(
	device: VkDevice,
	swapchainCount: uint32_t,
	pCreateInfos: *const VkSwapchainCreateInfoKHR,
	pAllocator: *const VkAllocationCallbacks,
	pSwapchains: *mut VkSwapchainKHR
) -> VkResult>;

extern "C" {
		pub fn vkGetDeviceQueue(
			device: VkDevice,
			queueFamilyIndex: uint32_t,
			queueIndex: uint32_t,
			pQueue: *mut VkQueue
		);
		
		pub fn vkGetDeviceProcAddr(
			device: VkDevice,
			pName: *const ::std::os::raw::c_char
		) -> PFN_vkVoidFunction;


		pub fn vkDestroyDevice(
			device: VkDevice,
			pAllocator: *const VkAllocationCallbacks
		);

		pub fn vkAllocateMemory(
			device: VkDevice,
			pAllocateInfo: *const VkMemoryAllocateInfo,
			pAllocator: *const VkAllocationCallbacks,
			pMemory: *mut VkDeviceMemory
		) -> VkResult;

		pub fn vkFreeMemory(
			device: VkDevice, memory: VkDeviceMemory,
			pAllocator: *const VkAllocationCallbacks
		);

		pub fn vkMapMemory(
			device: VkDevice, memory: VkDeviceMemory,
			offset: VkDeviceSize, size: VkDeviceSize,
			flags: VkMemoryMapFlags,
			ppData: *mut *mut ::std::os::raw::c_void
		) -> VkResult;

    pub fn vkUnmapMemory(device: VkDevice, memory: VkDeviceMemory);

		pub fn vkFlushMappedMemoryRanges(
			device: VkDevice,
			memoryRangeCount: uint32_t,
			pMemoryRanges: *const VkMappedMemoryRange
		) -> VkResult;

		pub fn vkInvalidateMappedMemoryRanges(
			device: VkDevice,
			memoryRangeCount: uint32_t,
			pMemoryRanges: *const VkMappedMemoryRange
		) -> VkResult;

    pub fn vkDeviceWaitIdle(device: VkDevice) -> VkResult;

		pub fn vkGetDeviceMemoryCommitment(
			device: VkDevice,
			memory: VkDeviceMemory,
			pCommittedMemoryInBytes: *mut VkDeviceSize
		);

		pub fn vkBindBufferMemory(
			device: VkDevice,
			buffer: VkBuffer,
			memory: VkDeviceMemory,
			memoryOffset: VkDeviceSize
		) -> VkResult;

		pub fn vkBindImageMemory(
			device: VkDevice,
			image: VkImage,
			memory: VkDeviceMemory,
			memoryOffset: VkDeviceSize
		) -> VkResult;

		pub fn vkGetBufferMemoryRequirements(
			device: VkDevice,
			buffer: VkBuffer,
			pMemoryRequirements: *mut VkMemoryRequirements
		);

		pub fn vkGetImageMemoryRequirements(
			device: VkDevice,
			image: VkImage,
			pMemoryRequirements: *mut VkMemoryRequirements
		);

		pub fn vkGetImageSparseMemoryRequirements(
			device: VkDevice,
			image: VkImage,
			pSparseMemoryRequirementCount: *mut uint32_t,
			pSparseMemoryRequirements: *mut VkSparseImageMemoryRequirements
		);
		
		pub fn vkCreateFence(
			device: VkDevice,
			pCreateInfo: *const VkFenceCreateInfo,
			pAllocator: *const VkAllocationCallbacks,
			pFence: *mut VkFence
		) -> VkResult;

		pub fn vkDestroyFence(
			device: VkDevice, fence: VkFence,
			pAllocator: *const VkAllocationCallbacks
		);

		pub fn vkResetFences(
			device: VkDevice,
			fenceCount: uint32_t,
			pFences: *const VkFence
		) -> VkResult;

    pub fn vkGetFenceStatus(
			device: VkDevice,
			fence: VkFence
		) -> VkResult;

		pub fn vkWaitForFences(
			device: VkDevice,
			fenceCount: uint32_t,
			pFences: *const VkFence,
			waitAll: VkBool32,
			timeout: uint64_t
		) -> VkResult;

		pub fn vkCreateSemaphore(
			device: VkDevice,
			pCreateInfo: *const VkSemaphoreCreateInfo,
			pAllocator: *const VkAllocationCallbacks,
			pSemaphore: *mut VkSemaphore
		) -> VkResult;

		pub fn vkDestroySemaphore(
			device: VkDevice,
			semaphore: VkSemaphore,
			pAllocator: *const VkAllocationCallbacks
		);

		pub fn vkCreateEvent(
			device: VkDevice,
			pCreateInfo: *const VkEventCreateInfo,
			pAllocator: *const VkAllocationCallbacks,
			pEvent: *mut VkEvent
		) -> VkResult;

		pub fn vkDestroyEvent(
			device: VkDevice,
			event: VkEvent,
			pAllocator: *const VkAllocationCallbacks
		);

    pub fn vkGetEventStatus(device: VkDevice, event: VkEvent) -> VkResult;

    pub fn vkSetEvent(device: VkDevice, event: VkEvent) -> VkResult;

    pub fn vkResetEvent(device: VkDevice, event: VkEvent) -> VkResult;

		pub fn vkCreateQueryPool(
			device: VkDevice,
			pCreateInfo: *const VkQueryPoolCreateInfo,
			pAllocator: *const VkAllocationCallbacks,
			pQueryPool: *mut VkQueryPool
		) -> VkResult;

		pub fn vkDestroyQueryPool(
			device: VkDevice,
			queryPool: VkQueryPool,
			pAllocator: *const VkAllocationCallbacks
		);

		pub fn vkGetQueryPoolResults(
			device: VkDevice, queryPool: VkQueryPool,
			firstQuery: uint32_t, queryCount: uint32_t,
			dataSize: size_t,
			pData: *mut ::std::os::raw::c_void,
			stride: VkDeviceSize,
			flags: VkQueryResultFlags
		) -> VkResult;

		pub fn vkCreateBuffer(
			device: VkDevice,
			pCreateInfo: *const VkBufferCreateInfo,
			pAllocator: *const VkAllocationCallbacks,
			pBuffer: *mut VkBuffer
		) -> VkResult;

		pub fn vkDestroyBuffer(
			device: VkDevice, buffer: VkBuffer,
			pAllocator: *const VkAllocationCallbacks
		);

		pub fn vkCreateBufferView(
			device: VkDevice,
			pCreateInfo: *const VkBufferViewCreateInfo,
			pAllocator: *const VkAllocationCallbacks,
			pView: *mut VkBufferView
		) -> VkResult;

		pub fn vkDestroyBufferView(
			device: VkDevice,
			bufferView: VkBufferView,
			pAllocator: *const VkAllocationCallbacks
		);

		pub fn vkCreateImage(
			device: VkDevice,
			pCreateInfo: *const VkImageCreateInfo,
			pAllocator: *const VkAllocationCallbacks,
			pImage: *mut VkImage
		) -> VkResult;

		pub fn vkDestroyImage(
			device: VkDevice,
			image: VkImage,
			pAllocator: *const VkAllocationCallbacks
		);

		pub fn vkGetImageSubresourceLayout(
			device: VkDevice,
			image: VkImage,
			pSubresource: *const VkImageSubresource,
			pLayout: *mut VkSubresourceLayout
		);
		
		pub fn vkCreateImageView(
			device: VkDevice,
			pCreateInfo: *const VkImageViewCreateInfo,
			pAllocator: *const VkAllocationCallbacks,
			pView: *mut VkImageView
		) -> VkResult;

		pub fn vkDestroyImageView(
			device: VkDevice,
			imageView: VkImageView,
			pAllocator: *const VkAllocationCallbacks
		);

		pub fn vkCreateShaderModule(
			device: VkDevice,
			pCreateInfo: *const VkShaderModuleCreateInfo,
			pAllocator: *const VkAllocationCallbacks,
			pShaderModule: *mut VkShaderModule
		) -> VkResult;

		pub fn vkDestroyShaderModule(
			device: VkDevice,
			shaderModule: VkShaderModule,
			pAllocator: *const VkAllocationCallbacks
		);

		pub fn vkCreatePipelineCache(
			device: VkDevice,
			pCreateInfo: *const VkPipelineCacheCreateInfo,
			pAllocator: *const VkAllocationCallbacks,
			pPipelineCache: *mut VkPipelineCache
		) -> VkResult;

		pub fn vkDestroyPipelineCache(
			device: VkDevice,
			pipelineCache: VkPipelineCache,
			pAllocator: *const VkAllocationCallbacks
		);

		pub fn vkGetPipelineCacheData(
			device: VkDevice,
			pipelineCache: VkPipelineCache,
			pDataSize: *mut size_t,
			pData: *mut ::std::os::raw::c_void
		) -> VkResult;

		pub fn vkMergePipelineCaches(
			device: VkDevice,
			dstCache: VkPipelineCache,
			srcCacheCount: uint32_t,
			pSrcCaches: *const VkPipelineCache
		) -> VkResult;

		pub fn vkCreateGraphicsPipelines(
			device: VkDevice,
			pipelineCache: VkPipelineCache,
			createInfoCount: uint32_t,
			pCreateInfos: *const VkGraphicsPipelineCreateInfo,
			pAllocator: *const VkAllocationCallbacks,
			pPipelines: *mut VkPipeline
		) -> VkResult;

		pub fn vkCreateComputePipelines(
			device: VkDevice,
			pipelineCache: VkPipelineCache,
			createInfoCount: uint32_t,
			pCreateInfos: *const VkComputePipelineCreateInfo,
			pAllocator: *const VkAllocationCallbacks,
			pPipelines: *mut VkPipeline
		) -> VkResult;

		pub fn vkDestroyPipeline(
			device: VkDevice,
			pipeline: VkPipeline,
			pAllocator: *const VkAllocationCallbacks
		);

		pub fn vkCreatePipelineLayout(
			device: VkDevice,
			pCreateInfo: *const VkPipelineLayoutCreateInfo,
			pAllocator: *const VkAllocationCallbacks,
			pPipelineLayout: *mut VkPipelineLayout
		) -> VkResult;

		pub fn vkDestroyPipelineLayout(
			device: VkDevice,
			pipelineLayout: VkPipelineLayout,
			pAllocator: *const VkAllocationCallbacks
		);

		pub fn vkCreateSampler(
			device: VkDevice,
			pCreateInfo: *const VkSamplerCreateInfo,
			pAllocator: *const VkAllocationCallbacks,
			pSampler: *mut VkSampler
		) -> VkResult;

		pub fn vkDestroySampler(
			device: VkDevice,
			sampler: VkSampler,
			pAllocator: *const VkAllocationCallbacks
		);

		pub fn vkCreateDescriptorSetLayout(
			device: VkDevice,
			pCreateInfo: *const VkDescriptorSetLayoutCreateInfo,
			pAllocator: *const VkAllocationCallbacks,
			pSetLayout: *mut VkDescriptorSetLayout
		) -> VkResult;

		pub fn vkDestroyDescriptorSetLayout(
			device: VkDevice,
			descriptorSetLayout:
			VkDescriptorSetLayout,
			pAllocator: *const VkAllocationCallbacks
		);

		pub fn vkCreateDescriptorPool(
			device: VkDevice,
			pCreateInfo: *const VkDescriptorPoolCreateInfo,
			pAllocator: *const VkAllocationCallbacks,
			pDescriptorPool: *mut VkDescriptorPool
		) -> VkResult;

		pub fn vkDestroyDescriptorPool(
			device: VkDevice,
			descriptorPool: VkDescriptorPool,
			pAllocator: *const VkAllocationCallbacks
		);

		pub fn vkResetDescriptorPool(
			device: VkDevice,
			descriptorPool: VkDescriptorPool,
			flags: VkDescriptorPoolResetFlags
		) -> VkResult;

		pub fn vkAllocateDescriptorSets(
			device: VkDevice,
			pAllocateInfo: *const VkDescriptorSetAllocateInfo,
			pDescriptorSets: *mut VkDescriptorSet
		) -> VkResult;

		pub fn vkFreeDescriptorSets(
			device: VkDevice,
			descriptorPool: VkDescriptorPool,
			descriptorSetCount: uint32_t,
			pDescriptorSets: *const VkDescriptorSet
		) -> VkResult;


		pub fn vkUpdateDescriptorSets(
			device: VkDevice,
			descriptorWriteCount: uint32_t,
			pDescriptorWrites: *const VkWriteDescriptorSet,
			descriptorCopyCount: uint32_t,
			pDescriptorCopies: *const VkCopyDescriptorSet
		);

		pub fn vkCreateFramebuffer(
			device: VkDevice,
			pCreateInfo: *const VkFramebufferCreateInfo,
			pAllocator: *const VkAllocationCallbacks,
			pFramebuffer: *mut VkFramebuffer
		) -> VkResult;

		pub fn vkDestroyFramebuffer(
			device: VkDevice,
			framebuffer: VkFramebuffer,
			pAllocator: *const VkAllocationCallbacks
		);

		pub fn vkCreateRenderPass(
			device: VkDevice,
			pCreateInfo: *const VkRenderPassCreateInfo,
			pAllocator: *const VkAllocationCallbacks,
			pRenderPass: *mut VkRenderPass
			) -> VkResult;

		pub fn vkDestroyRenderPass(
			device: VkDevice,
			renderPass: VkRenderPass,
			pAllocator: *const VkAllocationCallbacks
  	);

		pub fn vkGetRenderAreaGranularity(
			device: VkDevice,
			renderPass: VkRenderPass,
			pGranularity: *mut VkExtent2D
		);

		pub fn vkCreateCommandPool(
			device: VkDevice,
			pCreateInfo: *const VkCommandPoolCreateInfo,
			pAllocator: *const VkAllocationCallbacks,
			pCommandPool: *mut VkCommandPool
		) -> VkResult;

		pub fn vkDestroyCommandPool(
			device: VkDevice,
			commandPool: VkCommandPool,
			pAllocator: *const VkAllocationCallbacks
		);

		pub fn vkResetCommandPool(
			device: VkDevice,
			commandPool: VkCommandPool,
			flags: VkCommandPoolResetFlags
		) -> VkResult;

		pub fn vkAllocateCommandBuffers(
			device: VkDevice,
			pAllocateInfo: *const VkCommandBufferAllocateInfo,
			pCommandBuffers: *mut VkCommandBuffer
		) -> VkResult;

		pub fn vkFreeCommandBuffers(
			device: VkDevice, commandPool: VkCommandPool,
			commandBufferCount: uint32_t,
			pCommandBuffers: *const VkCommandBuffer
		);

		pub fn vkCreateSwapchainKHR(
			device: VkDevice,
			pCreateInfo: *const VkSwapchainCreateInfoKHR,
			pAllocator: *const VkAllocationCallbacks,
			pSwapchain: *mut VkSwapchainKHR
		) -> VkResult;

		pub fn vkDestroySwapchainKHR(
			device: VkDevice,
			swapchain: VkSwapchainKHR,
			pAllocator: *const VkAllocationCallbacks
		);

		pub fn vkGetSwapchainImagesKHR(
			device: VkDevice,
			swapchain: VkSwapchainKHR,
			pSwapchainImageCount: *mut uint32_t,
			pSwapchainImages: *mut VkImage
		) -> VkResult;

		pub fn vkAcquireNextImageKHR(
			device: VkDevice,
			swapchain: VkSwapchainKHR,
			timeout: uint64_t,
			semaphore: VkSemaphore,
			fence: VkFence,
			pImageIndex: *mut uint32_t
		) -> VkResult;

    pub fn vkCreateSharedSwapchainsKHR(
			device: VkDevice,
			swapchainCount: uint32_t,
			pCreateInfos: *const VkSwapchainCreateInfoKHR,
			pAllocator: *const VkAllocationCallbacks,
			pSwapchains: *mut VkSwapchainKHR
		) -> VkResult;
}
