
#![allow(non_snake_case)]
#![allow(non_camel_case_types)]
#![allow(dead_code)]

extern crate libc;
extern crate x11_dl;

use self::libc::uint8_t;
use self::libc::uint32_t;
use self::libc::int32_t;
use self::libc::size_t;

//use self::x11_dl::xlib;
use self::x11_dl::xlib::Display;
//use self::x11_dl::xlib::Window;
use self::x11_dl::xlib::VisualID;

use super::{
	VkFormat,
	VkFormatProperties,
	VkResult,
	VkBool32,

	VkSampleCountFlags,
	VkSampleCountFlagBits,

	VkExtensionProperties,

	VkLayerProperties,

	VkSurfaceKHR,
	VkSurfaceCapabilitiesKHR,
	VkSurfaceFormatKHR,
	
	VkPresentModeKHR,

	VkDisplayKHR,
	VkDisplayModeKHR,
	VkDisplayPropertiesKHR,
	VkDisplayPlanePropertiesKHR,
	VkDisplayPlaneCapabilitiesKHR,
	VkDisplayModePropertiesKHR,
	VkDisplayModeCreateInfoKHR,

};

use super::resource::{
	VkImageType,
	VkImageTiling,
	VkImageUsageFlags,
	VkImageCreateFlags,
	VkImageFormatProperties,
	VkSparseImageFormatProperties,
};

use super::queue::{
	VkQueueFamilyProperties,
};

use super::device::{
	VkDevice,
	VkDeviceSize,
	VkDeviceCreateInfo,
};

use super::memory::{
	VkMemoryType,
	VkMemoryHeap,
	VkAllocationCallbacks,
};

pub enum Struct_VkPhysicalDevice_T { }
pub type VkPhysicalDevice = *mut Struct_VkPhysicalDevice_T;

pub const VK_PHYSICAL_DEVICE_TYPE_BEGIN_RANGE: VkPhysicalDeviceType = VkPhysicalDeviceType::VK_PHYSICAL_DEVICE_TYPE_OTHER;
pub const VK_PHYSICAL_DEVICE_TYPE_END_RANGE: VkPhysicalDeviceType = VkPhysicalDeviceType::VK_PHYSICAL_DEVICE_TYPE_CPU;

pub type PFN_vkGetPhysicalDeviceFeatures = Option<unsafe extern "C" fn(
	physicalDevice: VkPhysicalDevice,
	pFeatures: *mut VkPhysicalDeviceFeatures
)>;

pub type PFN_vkGetPhysicalDeviceFormatProperties = Option<unsafe extern "C" fn(
	physicalDevice: VkPhysicalDevice,
	format: VkFormat,
	pFormatProperties: *mut VkFormatProperties
)>;

pub type PFN_vkGetPhysicalDeviceImageFormatProperties = Option<unsafe extern "C" fn(
	physicalDevice: VkPhysicalDevice,
	format: VkFormat,
	_type: VkImageType,
	tiling: VkImageTiling,
	usage: VkImageUsageFlags,
	flags: VkImageCreateFlags,
	pImageFormatProperties: *mut VkImageFormatProperties
) -> VkResult>;

pub type PFN_vkGetPhysicalDeviceProperties = Option<unsafe extern "C" fn(
	physicalDevice: VkPhysicalDevice,
	pProperties: *mut VkPhysicalDeviceProperties
)>;

pub type PFN_vkGetPhysicalDeviceQueueFamilyProperties = Option<unsafe extern "C" fn(
	physicalDevice: VkPhysicalDevice,
	pQueueFamilyPropertyCount: *mut uint32_t,
	pQueueFamilyProperties: *mut VkQueueFamilyProperties
)>;

pub type PFN_vkGetPhysicalDeviceMemoryProperties = Option<unsafe extern "C" fn(
	physicalDevice: VkPhysicalDevice,
	pMemoryProperties: *mut VkPhysicalDeviceMemoryProperties
)>;

pub type PFN_vkCreateDevice = Option<unsafe extern "C" fn(
	physicalDevice: VkPhysicalDevice,
	pCreateInfo: *const VkDeviceCreateInfo,
	pAllocator: *const VkAllocationCallbacks,
	pDevice: *mut VkDevice
) -> VkResult>;

pub type PFN_vkDestroyDevice = Option<unsafe extern "C" fn(
	device: VkDevice,
	pAllocator: *const VkAllocationCallbacks
)>;

pub type PFN_vkGetPhysicalDeviceSparseImageFormatProperties = Option<unsafe extern "C" fn(
	physicalDevice: VkPhysicalDevice,
	format: VkFormat,
	_type: VkImageType,
	samples: VkSampleCountFlagBits,
	usage: VkImageUsageFlags,
	tiling: VkImageTiling,
	pPropertyCount: *mut uint32_t,
	pProperties: *mut VkSparseImageFormatProperties
)>;

pub type PFN_vkEnumerateDeviceExtensionProperties = Option<unsafe extern "C" fn(
	physicalDevice: VkPhysicalDevice,
	pLayerName: *const ::std::os::raw::c_char,
	pPropertyCount: *mut uint32_t,
	pProperties: *mut VkExtensionProperties
) -> VkResult>;

pub type PFN_vkEnumerateDeviceLayerProperties = Option<unsafe extern "C" fn(
	physicalDevice: VkPhysicalDevice,
	pPropertyCount: *mut uint32_t,
	pProperties: *mut VkLayerProperties
) -> VkResult>;

pub type PFN_vkGetPhysicalDeviceSurfaceSupportKHR = Option<unsafe extern "C" fn(
	physicalDevice: VkPhysicalDevice,
	queueFamilyIndex: uint32_t,
	surface: VkSurfaceKHR,
	pSupported: *mut VkBool32
) -> VkResult>;

pub type PFN_vkGetPhysicalDeviceSurfaceCapabilitiesKHR = Option<unsafe extern "C" fn(
	physicalDevice: VkPhysicalDevice,
	surface: VkSurfaceKHR,
	pSurfaceCapabilities: *mut VkSurfaceCapabilitiesKHR
) -> VkResult>;

pub type PFN_vkGetPhysicalDeviceSurfaceFormatsKHR = Option<unsafe extern "C" fn(
	physicalDevice: VkPhysicalDevice,
	surface: VkSurfaceKHR,
	pSurfaceFormatCount: *mut uint32_t,
	pSurfaceFormats: *mut VkSurfaceFormatKHR
) -> VkResult>;

pub type PFN_vkGetPhysicalDeviceSurfacePresentModesKHR = Option<unsafe extern "C" fn(
	physicalDevice: VkPhysicalDevice,
	surface: VkSurfaceKHR,
	pPresentModeCount: *mut uint32_t,
	pPresentModes: *mut VkPresentModeKHR
) -> VkResult>;

pub type PFN_vkGetPhysicalDeviceDisplayPropertiesKHR = Option<unsafe extern "C" fn(
	physicalDevice: VkPhysicalDevice,
	pPropertyCount: *mut uint32_t,
	pProperties: *mut VkDisplayPropertiesKHR
) -> VkResult>;

pub type PFN_vkGetPhysicalDeviceDisplayPlanePropertiesKHR =Option<unsafe extern "C" fn(
	physicalDevice: VkPhysicalDevice,
	pPropertyCount: *mut uint32_t,
	pProperties: *mut VkDisplayPlanePropertiesKHR
) -> VkResult>;

pub type PFN_vkGetDisplayPlaneSupportedDisplaysKHR = Option<unsafe extern "C" fn(
	physicalDevice: VkPhysicalDevice,
	planeIndex: uint32_t,
	pDisplayCount: *mut uint32_t,
	pDisplays: *mut VkDisplayKHR
) -> VkResult>;

pub type PFN_vkGetDisplayModePropertiesKHR = Option<unsafe extern "C" fn(
	physicalDevice: VkPhysicalDevice,
	display: VkDisplayKHR,
	pPropertyCount: *mut uint32_t,
	pProperties: *mut VkDisplayModePropertiesKHR
) -> VkResult>;

pub type PFN_vkCreateDisplayModeKHR = Option<unsafe extern "C" fn(
	physicalDevice: VkPhysicalDevice,
	display: VkDisplayKHR,
	pCreateInfo: *const VkDisplayModeCreateInfoKHR,
	pAllocator: *const VkAllocationCallbacks,
	pMode: *mut VkDisplayModeKHR
) -> VkResult>;

pub type PFN_vkGetDisplayPlaneCapabilitiesKHR = Option<unsafe extern "C" fn(
	physicalDevice: VkPhysicalDevice,
	mode: VkDisplayModeKHR,
	planeIndex: uint32_t,
	pCapabilities: *mut VkDisplayPlaneCapabilitiesKHR
) -> VkResult>;

#[derive(Clone, Copy)]
#[repr(u32)]
pub enum VkPhysicalDeviceType {
    VK_PHYSICAL_DEVICE_TYPE_OTHER = 0,
    VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU = 1,
    VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU = 2,
    VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU = 3,
    VK_PHYSICAL_DEVICE_TYPE_CPU = 4,
    VK_PHYSICAL_DEVICE_TYPE_RANGE_SIZE = 5,
    VK_PHYSICAL_DEVICE_TYPE_MAX_ENUM = 2147483647,
}

#[repr(C)]
#[derive(Copy)]
pub struct VkPhysicalDeviceFeatures {
    pub robustBufferAccess: VkBool32,
    pub fullDrawIndexUint32: VkBool32,
    pub imageCubeArray: VkBool32,
    pub independentBlend: VkBool32,
    pub geometryShader: VkBool32,
    pub tessellationShader: VkBool32,
    pub sampleRateShading: VkBool32,
    pub dualSrcBlend: VkBool32,
    pub logicOp: VkBool32,
    pub multiDrawIndirect: VkBool32,
    pub drawIndirectFirstInstance: VkBool32,
    pub depthClamp: VkBool32,
    pub depthBiasClamp: VkBool32,
    pub fillModeNonSolid: VkBool32,
    pub depthBounds: VkBool32,
    pub wideLines: VkBool32,
    pub largePoints: VkBool32,
    pub alphaToOne: VkBool32,
    pub multiViewport: VkBool32,
    pub samplerAnisotropy: VkBool32,
    pub textureCompressionETC2: VkBool32,
    pub textureCompressionASTC_LDR: VkBool32,
    pub textureCompressionBC: VkBool32,
    pub occlusionQueryPrecise: VkBool32,
    pub pipelineStatisticsQuery: VkBool32,
    pub vertexPipelineStoresAndAtomics: VkBool32,
    pub fragmentStoresAndAtomics: VkBool32,
    pub shaderTessellationAndGeometryPointSize: VkBool32,
    pub shaderImageGatherExtended: VkBool32,
    pub shaderStorageImageExtendedFormats: VkBool32,
    pub shaderStorageImageMultisample: VkBool32,
    pub shaderStorageImageReadWithoutFormat: VkBool32,
    pub shaderStorageImageWriteWithoutFormat: VkBool32,
    pub shaderUniformBufferArrayDynamicIndexing: VkBool32,
    pub shaderSampledImageArrayDynamicIndexing: VkBool32,
    pub shaderStorageBufferArrayDynamicIndexing: VkBool32,
    pub shaderStorageImageArrayDynamicIndexing: VkBool32,
    pub shaderClipDistance: VkBool32,
    pub shaderCullDistance: VkBool32,
    pub shaderFloat64: VkBool32,
    pub shaderInt64: VkBool32,
    pub shaderInt16: VkBool32,
    pub shaderResourceResidency: VkBool32,
    pub shaderResourceMinLod: VkBool32,
    pub sparseBinding: VkBool32,
    pub sparseResidencyBuffer: VkBool32,
    pub sparseResidencyImage2D: VkBool32,
    pub sparseResidencyImage3D: VkBool32,
    pub sparseResidency2Samples: VkBool32,
    pub sparseResidency4Samples: VkBool32,
    pub sparseResidency8Samples: VkBool32,
    pub sparseResidency16Samples: VkBool32,
    pub sparseResidencyAliased: VkBool32,
    pub variableMultisampleRate: VkBool32,
    pub inheritedQueries: VkBool32,
}
impl Clone for VkPhysicalDeviceFeatures {
    fn clone(&self) -> Self { *self }
}
impl Default for VkPhysicalDeviceFeatures {
    fn default() -> Self { unsafe { ::std::mem::zeroed() } }
}

#[repr(C)]
#[derive(Copy)]
pub struct VkPhysicalDeviceLimits {
    pub maxImageDimension1D: uint32_t,
    pub maxImageDimension2D: uint32_t,
    pub maxImageDimension3D: uint32_t,
    pub maxImageDimensionCube: uint32_t,
    pub maxImageArrayLayers: uint32_t,
    pub maxTexelBufferElements: uint32_t,
    pub maxUniformBufferRange: uint32_t,
    pub maxStorageBufferRange: uint32_t,
    pub maxPushConstantsSize: uint32_t,
    pub maxMemoryAllocationCount: uint32_t,
    pub maxSamplerAllocationCount: uint32_t,
    pub bufferImageGranularity: VkDeviceSize,
    pub sparseAddressSpaceSize: VkDeviceSize,
    pub maxBoundDescriptorSets: uint32_t,
    pub maxPerStageDescriptorSamplers: uint32_t,
    pub maxPerStageDescriptorUniformBuffers: uint32_t,
    pub maxPerStageDescriptorStorageBuffers: uint32_t,
    pub maxPerStageDescriptorSampledImages: uint32_t,
    pub maxPerStageDescriptorStorageImages: uint32_t,
    pub maxPerStageDescriptorInputAttachments: uint32_t,
    pub maxPerStageResources: uint32_t,
    pub maxDescriptorSetSamplers: uint32_t,
    pub maxDescriptorSetUniformBuffers: uint32_t,
    pub maxDescriptorSetUniformBuffersDynamic: uint32_t,
    pub maxDescriptorSetStorageBuffers: uint32_t,
    pub maxDescriptorSetStorageBuffersDynamic: uint32_t,
    pub maxDescriptorSetSampledImages: uint32_t,
    pub maxDescriptorSetStorageImages: uint32_t,
    pub maxDescriptorSetInputAttachments: uint32_t,
    pub maxVertexInputAttributes: uint32_t,
    pub maxVertexInputBindings: uint32_t,
    pub maxVertexInputAttributeOffset: uint32_t,
    pub maxVertexInputBindingStride: uint32_t,
    pub maxVertexOutputComponents: uint32_t,
    pub maxTessellationGenerationLevel: uint32_t,
    pub maxTessellationPatchSize: uint32_t,
    pub maxTessellationControlPerVertexInputComponents: uint32_t,
    pub maxTessellationControlPerVertexOutputComponents: uint32_t,
    pub maxTessellationControlPerPatchOutputComponents: uint32_t,
    pub maxTessellationControlTotalOutputComponents: uint32_t,
    pub maxTessellationEvaluationInputComponents: uint32_t,
    pub maxTessellationEvaluationOutputComponents: uint32_t,
    pub maxGeometryShaderInvocations: uint32_t,
    pub maxGeometryInputComponents: uint32_t,
    pub maxGeometryOutputComponents: uint32_t,
    pub maxGeometryOutputVertices: uint32_t,
    pub maxGeometryTotalOutputComponents: uint32_t,
    pub maxFragmentInputComponents: uint32_t,
    pub maxFragmentOutputAttachments: uint32_t,
    pub maxFragmentDualSrcAttachments: uint32_t,
    pub maxFragmentCombinedOutputResources: uint32_t,
    pub maxComputeSharedMemorySize: uint32_t,
    pub maxComputeWorkGroupCount: [uint32_t; 3usize],
    pub maxComputeWorkGroupInvocations: uint32_t,
    pub maxComputeWorkGroupSize: [uint32_t; 3usize],
    pub subPixelPrecisionBits: uint32_t,
    pub subTexelPrecisionBits: uint32_t,
    pub mipmapPrecisionBits: uint32_t,
    pub maxDrawIndexedIndexValue: uint32_t,
    pub maxDrawIndirectCount: uint32_t,
    pub maxSamplerLodBias: ::std::os::raw::c_float,
    pub maxSamplerAnisotropy: ::std::os::raw::c_float,
    pub maxViewports: uint32_t,
    pub maxViewportDimensions: [uint32_t; 2usize],
    pub viewportBoundsRange: [::std::os::raw::c_float; 2usize],
    pub viewportSubPixelBits: uint32_t,
    pub minMemoryMapAlignment: size_t,
    pub minTexelBufferOffsetAlignment: VkDeviceSize,
    pub minUniformBufferOffsetAlignment: VkDeviceSize,
    pub minStorageBufferOffsetAlignment: VkDeviceSize,
    pub minTexelOffset: int32_t,
    pub maxTexelOffset: uint32_t,
    pub minTexelGatherOffset: int32_t,
    pub maxTexelGatherOffset: uint32_t,
    pub minInterpolationOffset: ::std::os::raw::c_float,
    pub maxInterpolationOffset: ::std::os::raw::c_float,
    pub subPixelInterpolationOffsetBits: uint32_t,
    pub maxFramebufferWidth: uint32_t,
    pub maxFramebufferHeight: uint32_t,
    pub maxFramebufferLayers: uint32_t,
    pub framebufferColorSampleCounts: VkSampleCountFlags,
    pub framebufferDepthSampleCounts: VkSampleCountFlags,
    pub framebufferStencilSampleCounts: VkSampleCountFlags,
    pub framebufferNoAttachmentsSampleCounts: VkSampleCountFlags,
    pub maxColorAttachments: uint32_t,
    pub sampledImageColorSampleCounts: VkSampleCountFlags,
    pub sampledImageIntegerSampleCounts: VkSampleCountFlags,
    pub sampledImageDepthSampleCounts: VkSampleCountFlags,
    pub sampledImageStencilSampleCounts: VkSampleCountFlags,
    pub storageImageSampleCounts: VkSampleCountFlags,
    pub maxSampleMaskWords: uint32_t,
    pub timestampComputeAndGraphics: VkBool32,
    pub timestampPeriod: ::std::os::raw::c_float,
    pub maxClipDistances: uint32_t,
    pub maxCullDistances: uint32_t,
    pub maxCombinedClipAndCullDistances: uint32_t,
    pub discreteQueuePriorities: uint32_t,
    pub pointSizeRange: [::std::os::raw::c_float; 2usize],
    pub lineWidthRange: [::std::os::raw::c_float; 2usize],
    pub pointSizeGranularity: ::std::os::raw::c_float,
    pub lineWidthGranularity: ::std::os::raw::c_float,
    pub strictLines: VkBool32,
    pub standardSampleLocations: VkBool32,
    pub optimalBufferCopyOffsetAlignment: VkDeviceSize,
    pub optimalBufferCopyRowPitchAlignment: VkDeviceSize,
    pub nonCoherentAtomSize: VkDeviceSize,
}
impl Clone for VkPhysicalDeviceLimits {
    fn clone(&self) -> Self { *self }
}
impl Default for VkPhysicalDeviceLimits {
    fn default() -> Self { unsafe { ::std::mem::zeroed() } }
}

#[repr(C)]
#[derive(Copy)]
pub struct VkPhysicalDeviceSparseProperties {
    pub residencyStandard2DBlockShape: VkBool32,
    pub residencyStandard2DMultisampleBlockShape: VkBool32,
    pub residencyStandard3DBlockShape: VkBool32,
    pub residencyAlignedMipSize: VkBool32,
    pub residencyNonResidentStrict: VkBool32,
}
impl Clone for VkPhysicalDeviceSparseProperties {
    fn clone(&self) -> Self { *self }
}
impl Default for VkPhysicalDeviceSparseProperties {
    fn default() -> Self { unsafe { ::std::mem::zeroed() } }
}

#[repr(C)]
#[derive(Copy)]
pub struct VkPhysicalDeviceProperties {
    pub apiVersion: uint32_t,
    pub driverVersion: uint32_t,
    pub vendorID: uint32_t,
    pub deviceID: uint32_t,
    pub deviceType: VkPhysicalDeviceType,
    pub deviceName: [::std::os::raw::c_char; 256usize],
    pub pipelineCacheUUID: [uint8_t; 16usize],
    pub limits: VkPhysicalDeviceLimits,
    pub sparseProperties: VkPhysicalDeviceSparseProperties,
}
impl Clone for VkPhysicalDeviceProperties {
    fn clone(&self) -> Self { *self }
}
impl Default for VkPhysicalDeviceProperties {
    fn default() -> Self { unsafe { ::std::mem::zeroed() } }
}

#[repr(C)]
#[derive(Copy)]
pub struct VkPhysicalDeviceMemoryProperties {
    pub memoryTypeCount: uint32_t,
    pub memoryTypes: [VkMemoryType; 32usize],
    pub memoryHeapCount: uint32_t,
    pub memoryHeaps: [VkMemoryHeap; 16usize],
}
impl Clone for VkPhysicalDeviceMemoryProperties {
    fn clone(&self) -> Self { *self }
}
impl Default for VkPhysicalDeviceMemoryProperties {
    fn default() -> Self { unsafe { ::std::mem::zeroed() } }
}

extern "C" {
    pub fn vkGetPhysicalDeviceFeatures(
			physicalDevice: VkPhysicalDevice,
			pFeatures: *mut VkPhysicalDeviceFeatures
		);
		
    pub fn vkGetPhysicalDeviceFormatProperties(
			physicalDevice: VkPhysicalDevice,
			format: VkFormat,
			pFormatProperties: *mut VkFormatProperties
		);
		
    pub fn vkGetPhysicalDeviceImageFormatProperties(
			physicalDevice: VkPhysicalDevice,
			format: VkFormat,
			_type: VkImageType,
			tiling: VkImageTiling,
			usage: VkImageUsageFlags,
			flags: VkImageCreateFlags,
			pImageFormatProperties: *mut VkImageFormatProperties
		) -> VkResult;

    pub fn vkGetPhysicalDeviceProperties(
			physicalDevice: VkPhysicalDevice,
      pProperties: *mut VkPhysicalDeviceProperties
		);

		pub fn vkGetPhysicalDeviceQueueFamilyProperties(
			physicalDevice: VkPhysicalDevice,
			pQueueFamilyPropertyCount: *mut uint32_t,
			pQueueFamilyProperties: *mut VkQueueFamilyProperties
		);

		pub fn vkGetPhysicalDeviceMemoryProperties(
			physicalDevice: VkPhysicalDevice,
			pMemoryProperties: *mut VkPhysicalDeviceMemoryProperties
		);

		pub fn vkCreateDevice(
			physicalDevice: VkPhysicalDevice,
			pCreateInfo: *const VkDeviceCreateInfo,
			pAllocator: *const VkAllocationCallbacks,
			pDevice: *mut VkDevice
		) -> VkResult;

		pub fn vkEnumerateDeviceExtensionProperties(
			physicalDevice: VkPhysicalDevice,
			pLayerName: *const ::std::os::raw::c_char,
			pPropertyCount: *mut uint32_t,
			pProperties: *mut VkExtensionProperties
		) -> VkResult;

		pub fn vkEnumerateDeviceLayerProperties(
			physicalDevice: VkPhysicalDevice,
			pPropertyCount: *mut uint32_t,
			pProperties: *mut VkLayerProperties
		) -> VkResult;

    pub fn vkGetPhysicalDeviceSparseImageFormatProperties(
			physicalDevice: VkPhysicalDevice,
			format: VkFormat,
			_type: VkImageType,
			samples: VkSampleCountFlagBits,
			usage: VkImageUsageFlags,
			tiling: VkImageTiling,
			pPropertyCount: *mut uint32_t,
			pProperties: *mut VkSparseImageFormatProperties
		);
		
		pub fn vkGetPhysicalDeviceSurfaceSupportKHR(
			physicalDevice:
			VkPhysicalDevice,
			queueFamilyIndex: uint32_t,
			surface: VkSurfaceKHR,
			pSupported: *mut VkBool32
		) -> VkResult;

		pub fn vkGetPhysicalDeviceSurfaceCapabilitiesKHR(
			physicalDevice: VkPhysicalDevice,
			surface: VkSurfaceKHR,
			pSurfaceCapabilities: *mut VkSurfaceCapabilitiesKHR
		) -> VkResult;

		pub fn vkGetPhysicalDeviceSurfaceFormatsKHR(
			physicalDevice: VkPhysicalDevice,
			surface: VkSurfaceKHR,
			pSurfaceFormatCount: *mut uint32_t,
			pSurfaceFormats: *mut VkSurfaceFormatKHR
		) -> VkResult;

		pub fn vkGetPhysicalDeviceSurfacePresentModesKHR(
			physicalDevice: VkPhysicalDevice,
			surface: VkSurfaceKHR,
			pPresentModeCount: *mut uint32_t,
			pPresentModes: *mut VkPresentModeKHR
		) -> VkResult;

		pub fn vkGetPhysicalDeviceDisplayPropertiesKHR(
			physicalDevice: VkPhysicalDevice,
			pPropertyCount: *mut uint32_t,
			pProperties: *mut VkDisplayPropertiesKHR
		) -> VkResult;

		pub fn vkGetPhysicalDeviceDisplayPlanePropertiesKHR(
			physicalDevice: VkPhysicalDevice,
			pPropertyCount: *mut uint32_t,
			pProperties: *mut VkDisplayPlanePropertiesKHR
		) -> VkResult;

    pub fn vkGetDisplayPlaneSupportedDisplaysKHR(
			physicalDevice: VkPhysicalDevice,
			planeIndex: uint32_t,
			pDisplayCount: *mut uint32_t,
			pDisplays: *mut VkDisplayKHR
		) -> VkResult;

    pub fn vkGetDisplayModePropertiesKHR(
			physicalDevice: VkPhysicalDevice,
			display: VkDisplayKHR,
			pPropertyCount: *mut uint32_t,
			pProperties: *mut VkDisplayModePropertiesKHR
		) -> VkResult;

    pub fn vkCreateDisplayModeKHR(
			physicalDevice: VkPhysicalDevice,
			display: VkDisplayKHR,
			pCreateInfo: *const VkDisplayModeCreateInfoKHR,
			pAllocator: *const VkAllocationCallbacks,
			pMode: *mut VkDisplayModeKHR
		) -> VkResult;
		
    pub fn vkGetDisplayPlaneCapabilitiesKHR(
			physicalDevice: VkPhysicalDevice,
			mode: VkDisplayModeKHR,
			planeIndex: uint32_t,
			pCapabilities: *mut VkDisplayPlaneCapabilitiesKHR
		) -> VkResult;

    pub fn vkGetPhysicalDeviceXlibPresentationSupportKHR(
			physicalDevice: VkPhysicalDevice,
			queueFamilyIndex: uint32_t,
			dpy: *mut Display,
			visualID: VisualID
		) -> VkBool32;
}
