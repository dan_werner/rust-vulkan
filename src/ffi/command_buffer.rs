#![allow(non_snake_case)]
#![allow(non_camel_case_types)]
#![allow(dead_code)]

extern crate libc;
use self::libc::uint32_t;
use self::libc::int32_t;

use super::{
	VkResult,
	VkFlags,
	VkStructureType,
	VkBool32,

	VkPipeline,
	VkPipelineLayout,
	VkPipelineBindPoint,

	VkDescriptorSet,

	VkViewport,
	VkRect2D,

	VkDependencyFlags,

	VkQueryPool,
	VkQueryResultFlags,
	VkQueryControlFlags,
	VkQueryPipelineStatisticFlags,

	VkEvent,
	VkPipelineStageFlags,
	VkPipelineStageFlagBits,

	VkShaderStageFlags,

	VkFilter,

	VkClearColorValue,
	VkClearDepthStencilValue,
	VkClearAttachment,
	VkClearRect,

	VkIndexType,

	VkStencilFaceFlags,


	VkRenderPass,
	VkRenderPassBeginInfo,
	VkSubpassContents,

	VkFramebuffer,
};

use super::resource::{
	VkImage,
	VkImageLayout,
	VkImageCopy,
	VkImageBlit,
	VkImageResolve,
	VkImageSubresourceRange,
	VkBuffer,
	VkBufferCopy,
	VkBufferImageCopy,
};

use super::device::{
	VkDeviceSize,
};

use super::memory::{
	VkMemoryBarrier,
	VkImageMemoryBarrier,
	VkBufferMemoryBarrier,
};

pub enum Struct_VkCommandBuffer_T { }
pub type VkCommandBuffer = *mut Struct_VkCommandBuffer_T;

pub enum Struct_VkCommandPool_T { }
pub type VkCommandPool = *mut Struct_VkCommandPool_T;

pub const VK_COMMAND_BUFFER_LEVEL_BEGIN_RANGE: VkCommandBufferLevel =
    VkCommandBufferLevel::VK_COMMAND_BUFFER_LEVEL_PRIMARY;
pub const VK_COMMAND_BUFFER_LEVEL_END_RANGE: VkCommandBufferLevel =
    VkCommandBufferLevel::VK_COMMAND_BUFFER_LEVEL_SECONDARY;

pub type PFN_vkBeginCommandBuffer = Option<unsafe extern "C" fn(
	commandBuffer: VkCommandBuffer,
	pBeginInfo: *const VkCommandBufferBeginInfo
) -> VkResult>;

pub type PFN_vkEndCommandBuffer =
	Option<extern "C" fn(commandBuffer: VkCommandBuffer) -> VkResult>;
pub type PFN_vkResetCommandBuffer =
	Option<extern "C" fn(commandBuffer: VkCommandBuffer, flags: VkCommandBufferResetFlags) -> VkResult>;

#[derive(Clone, Copy)]
#[repr(u32)]
pub enum VkCommandPoolCreateFlagBits {
    VK_COMMAND_POOL_CREATE_TRANSIENT_BIT = 1,
    VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT = 2,
}

pub type VkCommandPoolCreateFlags = VkFlags;

#[derive(Clone, Copy)]
#[repr(u32)]
pub enum VkCommandPoolResetFlagBits {
    VK_COMMAND_POOL_RESET_RELEASE_RESOURCES_BIT = 1,
    Dummy = 0,
}

pub type VkCommandPoolResetFlags = VkFlags;

#[derive(Clone, Copy)]
#[repr(u32)]
pub enum VkCommandBufferLevel {
    VK_COMMAND_BUFFER_LEVEL_PRIMARY = 0,
    VK_COMMAND_BUFFER_LEVEL_SECONDARY = 1,
    VK_COMMAND_BUFFER_LEVEL_RANGE_SIZE = 2,
    VK_COMMAND_BUFFER_LEVEL_MAX_ENUM = 2147483647,
}

#[derive(Clone, Copy)]
#[repr(u32)]
pub enum VkCommandBufferUsageFlagBits {
    VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT = 1,
    VK_COMMAND_BUFFER_USAGE_RENDER_PASS_CONTINUE_BIT = 2,
    VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT = 4,
}

pub type VkCommandBufferUsageFlags = VkFlags;

#[derive(Clone, Copy)]
#[repr(u32)]
pub enum VkCommandBufferResetFlagBits {
    VK_COMMAND_BUFFER_RESET_RELEASE_RESOURCES_BIT = 1,
    Dummy = 0,
}

pub type VkCommandBufferResetFlags = VkFlags;

#[repr(C)]
#[derive(Copy)]
pub struct VkCommandBufferAllocateInfo {
    pub sType: VkStructureType,
    pub pNext: *const ::std::os::raw::c_void,
    pub commandPool: VkCommandPool,
    pub level: VkCommandBufferLevel,
    pub commandBufferCount: uint32_t,
}
impl Clone for VkCommandBufferAllocateInfo {
    fn clone(&self) -> Self { *self }
}
impl Default for VkCommandBufferAllocateInfo {
    fn default() -> Self { unsafe { ::std::mem::zeroed() } }
}

#[repr(C)]
#[derive(Copy)]
pub struct VkCommandBufferInheritanceInfo {
    pub sType: VkStructureType,
    pub pNext: *const ::std::os::raw::c_void,
    pub renderPass: VkRenderPass,
    pub subpass: uint32_t,
    pub framebuffer: VkFramebuffer,
    pub occlusionQueryEnable: VkBool32,
    pub queryFlags: VkQueryControlFlags,
    pub pipelineStatistics: VkQueryPipelineStatisticFlags,
}
impl Clone for VkCommandBufferInheritanceInfo {
    fn clone(&self) -> Self { *self }
}
impl Default for VkCommandBufferInheritanceInfo {
    fn default() -> Self { unsafe { ::std::mem::zeroed() } }
}

#[repr(C)]
#[derive(Copy)]
pub struct VkCommandBufferBeginInfo {
    pub sType: VkStructureType,
    pub pNext: *const ::std::os::raw::c_void,
    pub flags: VkCommandBufferUsageFlags,
    pub pInheritanceInfo: *const VkCommandBufferInheritanceInfo,
}
impl Clone for VkCommandBufferBeginInfo {
    fn clone(&self) -> Self { *self }
}
impl Default for VkCommandBufferBeginInfo {
    fn default() -> Self { unsafe { ::std::mem::zeroed() } }
}

#[repr(C)]
#[derive(Copy)]
pub struct VkCommandPoolCreateInfo {
    pub sType: VkStructureType,
    pub pNext: *const ::std::os::raw::c_void,
    pub flags: VkCommandPoolCreateFlags,
    pub queueFamilyIndex: uint32_t,
}
impl Clone for VkCommandPoolCreateInfo {
    fn clone(&self) -> Self { *self }
}
impl Default for VkCommandPoolCreateInfo {
    fn default() -> Self { unsafe { ::std::mem::zeroed() } }
}

pub type PFN_vkCmdBindPipeline = Option<extern "C" fn(
	commandBuffer: VkCommandBuffer,
	pipelineBindPoint:
	VkPipelineBindPoint,
	pipeline: VkPipeline
)>;

pub type PFN_vkCmdSetViewport = Option<unsafe extern "C" fn(
	commandBuffer: VkCommandBuffer,
	firstViewport: uint32_t,
	viewportCount: uint32_t,
	pViewports: *const VkViewport
)>;

pub type PFN_vkCmdSetScissor = Option<unsafe extern "C" fn(
	commandBuffer: VkCommandBuffer,
	firstScissor: uint32_t,
	scissorCount: uint32_t,
	pScissors: *const VkRect2D
)>;

pub type PFN_vkCmdSetLineWidth = Option<extern "C" fn(
	commandBuffer: VkCommandBuffer,
	lineWidth: ::std::os::raw::c_float
)>;

pub type PFN_vkCmdSetDepthBias = Option<extern "C" fn(
	commandBuffer: VkCommandBuffer,
	depthBiasConstantFactor: ::std::os::raw::c_float,
	depthBiasClamp: ::std::os::raw::c_float,
	depthBiasSlopeFactor: ::std::os::raw::c_float
)>;

pub type PFN_vkCmdSetBlendConstants = Option<extern "C" fn(
	commandBuffer: VkCommandBuffer,
	blendConstants: *mut ::std::os::raw::c_float
)>;

pub type PFN_vkCmdSetDepthBounds = Option<extern "C" fn(
	commandBuffer: VkCommandBuffer,
	minDepthBounds: ::std::os::raw::c_float,
	maxDepthBounds: ::std::os::raw::c_float
)>;
		
pub type PFN_vkCmdSetStencilCompareMask = Option<extern "C" fn(
	commandBuffer: VkCommandBuffer,
	faceMask: VkStencilFaceFlags,
	compareMask: uint32_t
)>;

pub type PFN_vkCmdSetStencilWriteMask = Option<extern "C" fn(
	commandBuffer: VkCommandBuffer,
	faceMask: VkStencilFaceFlags,
	writeMask: uint32_t
)>;

pub type PFN_vkCmdSetStencilReference = Option<extern "C" fn(
	commandBuffer: VkCommandBuffer,
	faceMask: VkStencilFaceFlags,
	reference: uint32_t
)>;

pub type PFN_vkCmdBindDescriptorSets = Option<unsafe extern "C" fn(
	commandBuffer: VkCommandBuffer,
	pipelineBindPoint:
	VkPipelineBindPoint,
	layout: VkPipelineLayout,
	firstSet: uint32_t,
	descriptorSetCount: uint32_t,
	pDescriptorSets: *const VkDescriptorSet,
	dynamicOffsetCount: uint32_t,
	pDynamicOffsets: *const uint32_t
)>;

pub type PFN_vkCmdBindIndexBuffer = Option<extern "C" fn(
	commandBuffer: VkCommandBuffer,
	buffer: VkBuffer,
	offset: VkDeviceSize,
	indexType: VkIndexType
)>;

pub type PFN_vkCmdBindVertexBuffers = Option<unsafe extern "C" fn(
	commandBuffer: VkCommandBuffer,
	firstBinding: uint32_t,
	bindingCount: uint32_t,
	pBuffers: *const VkBuffer,
	pOffsets: *const VkDeviceSize
)>;

pub type PFN_vkCmdDraw = Option<extern "C" fn(
	commandBuffer: VkCommandBuffer,
	vertexCount: uint32_t,
	instanceCount: uint32_t,
	firstVertex: uint32_t,
	firstInstance: uint32_t
)>;

pub type PFN_vkCmdDrawIndexed = Option<extern "C" fn(
	commandBuffer: VkCommandBuffer,
	indexCount: uint32_t,
	instanceCount: uint32_t,
	firstIndex: uint32_t,
	vertexOffset: int32_t,
	firstInstance: uint32_t
)>;

pub type PFN_vkCmdDrawIndirect = Option<extern "C" fn(
	commandBuffer: VkCommandBuffer,
	buffer: VkBuffer,
	offset: VkDeviceSize,
	drawCount: uint32_t,
	stride: uint32_t
)>;

pub type PFN_vkCmdDrawIndexedIndirect = Option<extern "C" fn(
	commandBuffer: VkCommandBuffer,
	buffer: VkBuffer,
	offset: VkDeviceSize,
	drawCount: uint32_t,
	stride: uint32_t
)>;

pub type PFN_vkCmdDispatch = Option<extern "C" fn(
	commandBuffer: VkCommandBuffer,
	x: uint32_t, y: uint32_t,
	z: uint32_t
)>;

pub type PFN_vkCmdDispatchIndirect = Option<extern "C" fn(
	commandBuffer: VkCommandBuffer,
	buffer: VkBuffer,
	offset: VkDeviceSize
)>;

pub type PFN_vkCmdCopyBuffer = Option<unsafe extern "C" fn(
	commandBuffer: VkCommandBuffer,
	srcBuffer: VkBuffer,
	dstBuffer: VkBuffer,
	regionCount: uint32_t,
	pRegions: *const VkBufferCopy
)>;

pub type PFN_vkCmdCopyImage = Option<unsafe extern "C" fn(
	commandBuffer: VkCommandBuffer,
	srcImage: VkImage,
	srcImageLayout: VkImageLayout,
	dstImage: VkImage,
	dstImageLayout: VkImageLayout,
	regionCount: uint32_t,
	pRegions: *const VkImageCopy
)>;

pub type PFN_vkCmdBlitImage = Option<unsafe extern "C" fn(
	commandBuffer: VkCommandBuffer,
	srcImage: VkImage,
	srcImageLayout: VkImageLayout,
	dstImage: VkImage,
	dstImageLayout: VkImageLayout,
	regionCount: uint32_t,
	pRegions: *const VkImageBlit,
	filter: VkFilter
)>;

pub type PFN_vkCmdCopyBufferToImage = Option<unsafe extern "C" fn(
	commandBuffer: VkCommandBuffer,
	srcBuffer: VkBuffer,
	dstImage: VkImage,
	dstImageLayout: VkImageLayout,
	regionCount: uint32_t,
	pRegions: *const VkBufferImageCopy
)>;

pub type PFN_vkCmdCopyImageToBuffer = Option<unsafe extern "C" fn(
	commandBuffer: VkCommandBuffer,
	srcImage: VkImage,
	srcImageLayout: VkImageLayout,
	dstBuffer: VkBuffer,
	regionCount: uint32_t,
	pRegions: *const VkBufferImageCopy
)>;

pub type PFN_vkCmdUpdateBuffer = Option<unsafe extern "C" fn(
	commandBuffer: VkCommandBuffer,
	dstBuffer: VkBuffer,
	dstOffset: VkDeviceSize,
	dataSize: VkDeviceSize,
	pData: *const uint32_t
)>;

pub type PFN_vkCmdFillBuffer = Option<extern "C" fn(
	commandBuffer: VkCommandBuffer,
	dstBuffer: VkBuffer,
	dstOffset: VkDeviceSize,
	size: VkDeviceSize, data: uint32_t
)>;

pub type PFN_vkCmdClearColorImage = Option<unsafe extern "C" fn(
	commandBuffer: VkCommandBuffer,
	image: VkImage,
	imageLayout: VkImageLayout,
	pColor: *const VkClearColorValue,
	rangeCount: uint32_t,
	pRanges: *const VkImageSubresourceRange
)>;

pub type PFN_vkCmdClearDepthStencilImage = Option<unsafe extern "C" fn(
	commandBuffer: VkCommandBuffer,
	image: VkImage,
	imageLayout: VkImageLayout,
	pDepthStencil: *const VkClearDepthStencilValue,
	rangeCount: uint32_t,
	pRanges: *const VkImageSubresourceRange
)>;

pub type PFN_vkCmdClearAttachments = Option<unsafe extern "C" fn(
	commandBuffer: VkCommandBuffer,
	attachmentCount: uint32_t,
	pAttachments: *const VkClearAttachment,
	rectCount: uint32_t,
	pRects: *const VkClearRect
)>;

pub type PFN_vkCmdResolveImage = Option<unsafe extern "C" fn(
	commandBuffer: VkCommandBuffer,
	srcImage: VkImage,
	srcImageLayout: VkImageLayout,
	dstImage: VkImage,
	dstImageLayout: VkImageLayout,
	regionCount: uint32_t,
	pRegions: *const VkImageResolve
)>;

pub type PFN_vkCmdSetEvent = Option<extern "C" fn(
	commandBuffer: VkCommandBuffer,
	event: VkEvent,
	stageMask: VkPipelineStageFlags
)>;

pub type PFN_vkCmdResetEvent = Option<extern "C" fn(
	commandBuffer: VkCommandBuffer,
	event: VkEvent,
	stageMask: VkPipelineStageFlags
)>;

pub type PFN_vkCmdWaitEvents = Option<unsafe extern "C" fn(
	commandBuffer: VkCommandBuffer,
	eventCount: uint32_t,
	pEvents: *const VkEvent,
	srcStageMask: VkPipelineStageFlags,
	dstStageMask: VkPipelineStageFlags,
	memoryBarrierCount: uint32_t,
	pMemoryBarriers: *const VkMemoryBarrier,
	bufferMemoryBarrierCount: uint32_t,
	pBufferMemoryBarriers: *const VkBufferMemoryBarrier,
	imageMemoryBarrierCount: uint32_t,
	pImageMemoryBarriers: *const VkImageMemoryBarrier
)>;

pub type PFN_vkCmdPipelineBarrier = Option<unsafe extern "C" fn(
	commandBuffer: VkCommandBuffer,
	srcStageMask: VkPipelineStageFlags,
	dstStageMask: VkPipelineStageFlags,
	dependencyFlags: VkDependencyFlags,
	memoryBarrierCount: uint32_t,
	pMemoryBarriers: *const VkMemoryBarrier,
	bufferMemoryBarrierCount: uint32_t,
	pBufferMemoryBarriers: *const VkBufferMemoryBarrier,
	imageMemoryBarrierCount: uint32_t,
	pImageMemoryBarriers: *const VkImageMemoryBarrier
)>;

pub type PFN_vkCmdBeginQuery = Option<extern "C" fn(
	commandBuffer: VkCommandBuffer,
	queryPool: VkQueryPool,
	query: uint32_t,
	flags: VkQueryControlFlags
)>;

pub type PFN_vkCmdEndQuery = Option<extern "C" fn(
	commandBuffer: VkCommandBuffer,
	queryPool: VkQueryPool,
	query: uint32_t
)>;

pub type PFN_vkCmdResetQueryPool = Option<extern "C" fn(
	commandBuffer: VkCommandBuffer,
	queryPool: VkQueryPool,
	firstQuery: uint32_t,
	queryCount: uint32_t
)>;

pub type PFN_vkCmdWriteTimestamp = Option<extern "C" fn(
	commandBuffer: VkCommandBuffer,
	pipelineStage: VkPipelineStageFlagBits,
	queryPool: VkQueryPool,
	query: uint32_t
)>;

pub type PFN_vkCmdCopyQueryPoolResults = Option<extern "C" fn(
	commandBuffer: VkCommandBuffer,
	queryPool: VkQueryPool,
	firstQuery: uint32_t,
	queryCount: uint32_t,
	dstBuffer: VkBuffer,
	dstOffset: VkDeviceSize,
	stride: VkDeviceSize,
	flags: VkQueryResultFlags
)>;

pub type PFN_vkCmdPushConstants = Option<unsafe extern "C" fn(
	commandBuffer: VkCommandBuffer,
	layout: VkPipelineLayout,
	stageFlags: VkShaderStageFlags,
	offset: uint32_t,
	size: uint32_t,
	pValues: *const ::std::os::raw::c_void
)>;

pub type PFN_vkCmdBeginRenderPass = Option<unsafe extern "C" fn(
	commandBuffer: VkCommandBuffer,
	pRenderPassBegin: *const VkRenderPassBeginInfo,
	contents: VkSubpassContents
)>;

pub type PFN_vkCmdNextSubpass =
    Option<extern "C" fn(commandBuffer: VkCommandBuffer, contents: VkSubpassContents)>;

pub type PFN_vkCmdEndRenderPass =
    Option<extern "C" fn(commandBuffer: VkCommandBuffer)>;

pub type PFN_vkCmdExecuteCommands = Option<unsafe extern "C" fn(
	commandBuffer: VkCommandBuffer,
	commandBufferCount: uint32_t,
	pCommandBuffers: *const VkCommandBuffer
)>;


extern "C" {

		pub fn vkBeginCommandBuffer(
			commandBuffer: VkCommandBuffer,
			pBeginInfo: *const VkCommandBufferBeginInfo
		) -> VkResult;

		pub fn vkEndCommandBuffer(commandBuffer: VkCommandBuffer) -> VkResult;

		pub fn vkResetCommandBuffer(
			commandBuffer: VkCommandBuffer,
			flags: VkCommandBufferResetFlags
		) -> VkResult;

		pub fn vkCmdBindPipeline(
			commandBuffer: VkCommandBuffer,
			pipelineBindPoint: VkPipelineBindPoint,
			pipeline: VkPipeline
		);

		pub fn vkCmdSetViewport(
			commandBuffer: VkCommandBuffer,
			firstViewport: uint32_t,
			viewportCount: uint32_t,
			pViewports: *const VkViewport
		);

		pub fn vkCmdSetScissor(
			commandBuffer: VkCommandBuffer,
			firstScissor: uint32_t,
			scissorCount: uint32_t,
			pScissors: *const VkRect2D
		);

		pub fn vkCmdSetLineWidth(
			commandBuffer: VkCommandBuffer,
			lineWidth: ::std::os::raw::c_float
		);

		pub fn vkCmdSetDepthBias(
			commandBuffer: VkCommandBuffer,
			depthBiasConstantFactor: ::std::os::raw::c_float,
			depthBiasClamp: ::std::os::raw::c_float,
			depthBiasSlopeFactor: ::std::os::raw::c_float
		);

		pub fn vkCmdSetBlendConstants(
			commandBuffer: VkCommandBuffer,
			blendConstants: *mut ::std::os::raw::c_float
		);

		pub fn vkCmdSetDepthBounds(
			commandBuffer: VkCommandBuffer,
			minDepthBounds: ::std::os::raw::c_float,
			maxDepthBounds: ::std::os::raw::c_float
		);

		pub fn vkCmdSetStencilCompareMask(
			commandBuffer: VkCommandBuffer,
			faceMask: VkStencilFaceFlags,
			compareMask: uint32_t
		);
		
		pub fn vkCmdSetStencilWriteMask(
			commandBuffer: VkCommandBuffer,
			faceMask: VkStencilFaceFlags,
			writeMask: uint32_t
		);

		pub fn vkCmdSetStencilReference(
			commandBuffer: VkCommandBuffer,
			faceMask: VkStencilFaceFlags,
			reference: uint32_t
		);

		pub fn vkCmdBindDescriptorSets(
			commandBuffer: VkCommandBuffer,
			pipelineBindPoint: VkPipelineBindPoint,
			layout: VkPipelineLayout,
			firstSet: uint32_t,
			descriptorSetCount: uint32_t,
			pDescriptorSets: *const VkDescriptorSet,
			dynamicOffsetCount: uint32_t,
			pDynamicOffsets: *const uint32_t
		);

		pub fn vkCmdBindIndexBuffer(
			commandBuffer: VkCommandBuffer,
			buffer: VkBuffer,
			offset: VkDeviceSize,
			indexType: VkIndexType
		);

		pub fn vkCmdBindVertexBuffers(
			commandBuffer: VkCommandBuffer,
			firstBinding: uint32_t,
			bindingCount: uint32_t,
			pBuffers: *const VkBuffer,
			pOffsets: *const VkDeviceSize
		);

		pub fn vkCmdDraw(
			commandBuffer: VkCommandBuffer,
			vertexCount: uint32_t,
			instanceCount: uint32_t,
			firstVertex: uint32_t,
			firstInstance: uint32_t
		);

		pub fn vkCmdDrawIndexed(
			commandBuffer: VkCommandBuffer,
			indexCount: uint32_t,
			instanceCount: uint32_t,
			firstIndex: uint32_t,
			vertexOffset: int32_t,
			firstInstance: uint32_t
		);

		pub fn vkCmdDrawIndirect(
			commandBuffer: VkCommandBuffer,
			buffer: VkBuffer,
			offset: VkDeviceSize,
			drawCount: uint32_t,
			stride: uint32_t
			);

		pub fn vkCmdDrawIndexedIndirect(
			commandBuffer: VkCommandBuffer,
			buffer: VkBuffer,
			offset: VkDeviceSize,
			drawCount: uint32_t,
			stride: uint32_t
		);

		pub fn vkCmdDispatch(
			commandBuffer: VkCommandBuffer,
			x: uint32_t,
			y: uint32_t,
			z: uint32_t
		);

		pub fn vkCmdDispatchIndirect(
			commandBuffer: VkCommandBuffer,
			buffer: VkBuffer,
			offset: VkDeviceSize
		);

		pub fn vkCmdCopyBuffer(
			commandBuffer: VkCommandBuffer,
			srcBuffer: VkBuffer,
			dstBuffer: VkBuffer,
			regionCount: uint32_t,
			pRegions: *const VkBufferCopy
		);

		pub fn vkCmdCopyImage(
			commandBuffer: VkCommandBuffer,
			srcImage: VkImage,
			srcImageLayout: VkImageLayout,
			dstImage: VkImage,
			dstImageLayout: VkImageLayout,
			regionCount: uint32_t,
			pRegions: *const VkImageCopy
		);

		pub fn vkCmdBlitImage(
			commandBuffer: VkCommandBuffer,
			srcImage: VkImage,
			srcImageLayout: VkImageLayout,
			dstImage: VkImage,
			dstImageLayout: VkImageLayout,
			regionCount: uint32_t,
			pRegions: *const VkImageBlit,
			filter: VkFilter
		);

		pub fn vkCmdCopyBufferToImage(
			commandBuffer: VkCommandBuffer,
			srcBuffer: VkBuffer,
			dstImage: VkImage,
			dstImageLayout: VkImageLayout,
			regionCount: uint32_t,
			pRegions: *const VkBufferImageCopy
		);

		pub fn vkCmdCopyImageToBuffer(
			commandBuffer: VkCommandBuffer,
			srcImage: VkImage,
			srcImageLayout: VkImageLayout,
			dstBuffer: VkBuffer, regionCount: uint32_t,
			pRegions: *const VkBufferImageCopy
		);

		pub fn vkCmdUpdateBuffer(
			commandBuffer: VkCommandBuffer,
			dstBuffer: VkBuffer, dstOffset: VkDeviceSize,
			dataSize: VkDeviceSize, pData: *const uint32_t
			);

		pub fn vkCmdFillBuffer(
			commandBuffer: VkCommandBuffer,
			dstBuffer: VkBuffer,
			dstOffset: VkDeviceSize,
			size: VkDeviceSize,
			data: uint32_t
		);

		pub fn vkCmdClearColorImage(
			commandBuffer: VkCommandBuffer,
			image: VkImage,
			imageLayout: VkImageLayout,
			pColor: *const VkClearColorValue,
			rangeCount: uint32_t,
			pRanges: *const VkImageSubresourceRange
		);

		pub fn vkCmdClearDepthStencilImage(
			commandBuffer: VkCommandBuffer,
			image: VkImage,
			imageLayout: VkImageLayout,
			pDepthStencil: *const VkClearDepthStencilValue,
			rangeCount: uint32_t,
			pRanges: *const VkImageSubresourceRange
		);

		pub fn vkCmdClearAttachments(
			commandBuffer: VkCommandBuffer,
			attachmentCount: uint32_t,
			pAttachments: *const VkClearAttachment,
			rectCount: uint32_t,
			pRects: *const VkClearRect
		);

		pub fn vkCmdResolveImage(
			commandBuffer: VkCommandBuffer,
			srcImage: VkImage, srcImageLayout: VkImageLayout,
			dstImage: VkImage, dstImageLayout: VkImageLayout,
			regionCount: uint32_t,
			pRegions: *const VkImageResolve
		);

		pub fn vkCmdSetEvent(
			commandBuffer: VkCommandBuffer,
			event: VkEvent,
			stageMask: VkPipelineStageFlags
		);

		pub fn vkCmdResetEvent(
			commandBuffer: VkCommandBuffer, event: VkEvent,
			stageMask: VkPipelineStageFlags
		);

		pub fn vkCmdWaitEvents(
			commandBuffer: VkCommandBuffer,
			eventCount: uint32_t, pEvents: *const VkEvent,
			srcStageMask: VkPipelineStageFlags,
			dstStageMask: VkPipelineStageFlags,
			memoryBarrierCount: uint32_t,
			pMemoryBarriers: *const VkMemoryBarrier,
			bufferMemoryBarrierCount: uint32_t,
			pBufferMemoryBarriers: *const VkBufferMemoryBarrier,
			imageMemoryBarrierCount: uint32_t,
			pImageMemoryBarriers: *const VkImageMemoryBarrier
		);

		pub fn vkCmdPipelineBarrier(
			commandBuffer: VkCommandBuffer,
			srcStageMask: VkPipelineStageFlags,
			dstStageMask: VkPipelineStageFlags,
			dependencyFlags: VkDependencyFlags,
			memoryBarrierCount: uint32_t,
			pMemoryBarriers: *const VkMemoryBarrier,
			bufferMemoryBarrierCount: uint32_t,
			pBufferMemoryBarriers: *const VkBufferMemoryBarrier,
			imageMemoryBarrierCount: uint32_t,
			pImageMemoryBarriers: *const VkImageMemoryBarrier
		);

		pub fn vkCmdBeginQuery(
			commandBuffer: VkCommandBuffer,
			queryPool: VkQueryPool, query: uint32_t,
			flags: VkQueryControlFlags
		);

		pub fn vkCmdEndQuery(
			commandBuffer: VkCommandBuffer,
			queryPool: VkQueryPool, query: uint32_t
		);

		pub fn vkCmdResetQueryPool(
			commandBuffer: VkCommandBuffer,
			queryPool: VkQueryPool, firstQuery: uint32_t,
			queryCount: uint32_t
			);

		pub fn vkCmdWriteTimestamp(
			commandBuffer: VkCommandBuffer,
			pipelineStage: VkPipelineStageFlagBits,
			queryPool: VkQueryPool, query: uint32_t
		);

		pub fn vkCmdCopyQueryPoolResults(
			commandBuffer: VkCommandBuffer,
			queryPool: VkQueryPool,
			firstQuery: uint32_t,
			queryCount: uint32_t,
			dstBuffer: VkBuffer,
			dstOffset: VkDeviceSize,
			stride: VkDeviceSize,
			flags: VkQueryResultFlags
		);

		pub fn vkCmdPushConstants(
			commandBuffer: VkCommandBuffer,
			layout: VkPipelineLayout,
			stageFlags: VkShaderStageFlags,
			offset: uint32_t, size: uint32_t,
			pValues: *const ::std::os::raw::c_void
		);

		pub fn vkCmdBeginRenderPass(
			commandBuffer: VkCommandBuffer,
			pRenderPassBegin: *const VkRenderPassBeginInfo,
			contents: VkSubpassContents
		);

		pub fn vkCmdNextSubpass(
			commandBuffer: VkCommandBuffer, contents: VkSubpassContents);

		pub fn vkCmdEndRenderPass(commandBuffer: VkCommandBuffer);

		pub fn vkCmdExecuteCommands(
			commandBuffer: VkCommandBuffer,
			commandBufferCount: uint32_t,
			pCommandBuffers: *const VkCommandBuffer
		);
}
