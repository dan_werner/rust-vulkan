#![allow(non_snake_case)]
#![allow(non_camel_case_types)]
#![allow(dead_code)]

extern crate libc;
use self::libc::uint32_t;

use super::{
	VkFlags,
	VkExtent3D,
	VkOffset3D,
	VkStructureType,

	//memory?
	VkSharingMode,
	VkComponentMapping,

	//Move in candidates:
	VkFormat,
	VkSampleCountFlags,
	VkSampleCountFlagBits,
};

use super::device::{
	VkDeviceSize,
	
};

pub type VkBufferUsageFlags = VkFlags;
pub type VkBufferViewCreateFlags = VkFlags;

pub const VK_IMAGE_TYPE_BEGIN_RANGE: VkImageType = VkImageType::VK_IMAGE_TYPE_1D;
pub const VK_IMAGE_TYPE_END_RANGE: VkImageType = VkImageType::VK_IMAGE_TYPE_3D;

pub enum Struct_VkBuffer_T { }
pub type VkBuffer = *mut Struct_VkBuffer_T;

pub enum Struct_VkImage_T { }
pub type VkImage = *mut Struct_VkImage_T;

pub enum Struct_VkBufferView_T { }
pub type VkBufferView = *mut Struct_VkBufferView_T;

pub enum Struct_VkImageView_T { }
pub type VkImageView = *mut Struct_VkImageView_T;

#[derive(Clone, Copy)]
#[repr(u32)]
pub enum VkImageType {
    VK_IMAGE_TYPE_1D = 0,
    VK_IMAGE_TYPE_2D = 1,
    VK_IMAGE_TYPE_3D = 2,
    VK_IMAGE_TYPE_RANGE_SIZE = 3,
    VK_IMAGE_TYPE_MAX_ENUM = 2147483647,
}

pub const VK_IMAGE_TILING_BEGIN_RANGE: VkImageTiling = VkImageTiling::VK_IMAGE_TILING_OPTIMAL;
pub const VK_IMAGE_TILING_END_RANGE: VkImageTiling = VkImageTiling::VK_IMAGE_TILING_LINEAR;

#[derive(Clone, Copy)]
#[repr(u32)]
pub enum VkImageTiling {
    VK_IMAGE_TILING_OPTIMAL = 0,
    VK_IMAGE_TILING_LINEAR = 1,
    VK_IMAGE_TILING_RANGE_SIZE = 2,
    VK_IMAGE_TILING_MAX_ENUM = 2147483647,
}

#[derive(Clone, Copy, PartialEq, Eq)]
#[repr(u32)]
pub enum VkImageLayout {
    VK_IMAGE_LAYOUT_UNDEFINED = 0,
    VK_IMAGE_LAYOUT_GENERAL = 1,
    VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL = 2,
    VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL = 3,
    VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL = 4,
    VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL = 5,
    VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL = 6,
    VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL = 7,
    VK_IMAGE_LAYOUT_PREINITIALIZED = 8,
    VK_IMAGE_LAYOUT_PRESENT_SRC_KHR = 1000001002,
    VK_IMAGE_LAYOUT_RANGE_SIZE = 9,
    VK_IMAGE_LAYOUT_MAX_ENUM = 2147483647,
}
pub const VK_IMAGE_VIEW_TYPE_BEGIN_RANGE: VkImageViewType = VkImageViewType::VK_IMAGE_VIEW_TYPE_1D;
pub const VK_IMAGE_VIEW_TYPE_END_RANGE: VkImageViewType = VkImageViewType::VK_IMAGE_VIEW_TYPE_CUBE_ARRAY;

#[derive(Clone, Copy)]
#[repr(u32)]
pub enum VkImageViewType {
    VK_IMAGE_VIEW_TYPE_1D = 0,
    VK_IMAGE_VIEW_TYPE_2D = 1,
    VK_IMAGE_VIEW_TYPE_3D = 2,
    VK_IMAGE_VIEW_TYPE_CUBE = 3,
    VK_IMAGE_VIEW_TYPE_1D_ARRAY = 4,
    VK_IMAGE_VIEW_TYPE_2D_ARRAY = 5,
    VK_IMAGE_VIEW_TYPE_CUBE_ARRAY = 6,
    VK_IMAGE_VIEW_TYPE_RANGE_SIZE = 7,
    VK_IMAGE_VIEW_TYPE_MAX_ENUM = 2147483647,
}

#[derive(Clone, Copy)]
#[repr(u32)]
pub enum VkImageUsageFlagBits {
    VK_IMAGE_USAGE_TRANSFER_SRC_BIT = 1,
    VK_IMAGE_USAGE_TRANSFER_DST_BIT = 2,
    VK_IMAGE_USAGE_SAMPLED_BIT = 4,
    VK_IMAGE_USAGE_STORAGE_BIT = 8,
    VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT = 16,
    VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT = 32,
    VK_IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT = 64,
    VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT = 128,
}

pub type VkImageUsageFlags = VkFlags;

#[derive(Clone, Copy)]
#[repr(u32)]
pub enum VkImageCreateFlagBits {
    VK_IMAGE_CREATE_SPARSE_BINDING_BIT = 1,
    VK_IMAGE_CREATE_SPARSE_RESIDENCY_BIT = 2,
    VK_IMAGE_CREATE_SPARSE_ALIASED_BIT = 4,
    VK_IMAGE_CREATE_MUTABLE_FORMAT_BIT = 8,
    VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT = 16,
}

pub type VkImageCreateFlags = VkFlags;

#[derive(Clone, Copy)]
#[repr(u32)]
pub enum VkImageAspectFlagBits {
    VK_IMAGE_ASPECT_COLOR_BIT = 1,
    VK_IMAGE_ASPECT_DEPTH_BIT = 2,
    VK_IMAGE_ASPECT_STENCIL_BIT = 4,
    VK_IMAGE_ASPECT_METADATA_BIT = 8,
}

pub type VkImageAspectFlags = VkFlags;

pub type VkImageViewCreateFlags = VkFlags;

#[repr(C)]
#[derive(Copy)]
pub struct VkImageFormatProperties {
    pub maxExtent: VkExtent3D,
    pub maxMipLevels: uint32_t,
    pub maxArrayLayers: uint32_t,
    pub sampleCounts: VkSampleCountFlags,
    pub maxResourceSize: VkDeviceSize,
}
impl Clone for VkImageFormatProperties {
    fn clone(&self) -> Self { *self }
}
impl Default for VkImageFormatProperties {
    fn default() -> Self { unsafe { ::std::mem::zeroed() } }
}

#[repr(C)]
#[derive(Copy)]
pub struct VkImageSubresource {
    pub aspectMask: VkImageAspectFlags,
    pub mipLevel: uint32_t,
    pub arrayLayer: uint32_t,
}
impl Clone for VkImageSubresource {
    fn clone(&self) -> Self { *self }
}
impl Default for VkImageSubresource {
    fn default() -> Self { unsafe { ::std::mem::zeroed() } }
}

#[repr(C)]
#[derive(Copy)]
pub struct VkImageCreateInfo {
    pub sType: VkStructureType,
    pub pNext: *const ::std::os::raw::c_void,
    pub flags: VkImageCreateFlags,
    pub imageType: VkImageType,
    pub format: VkFormat,
    pub extent: VkExtent3D,
    pub mipLevels: uint32_t,
    pub arrayLayers: uint32_t,
    pub samples: VkSampleCountFlagBits,
    pub tiling: VkImageTiling,
    pub usage: VkImageUsageFlags,
    pub sharingMode: VkSharingMode,
    pub queueFamilyIndexCount: uint32_t,
    pub pQueueFamilyIndices: *const uint32_t,
    pub initialLayout: VkImageLayout,
}
impl Clone for VkImageCreateInfo {
    fn clone(&self) -> Self { *self }
}
impl Default for VkImageCreateInfo {
    fn default() -> Self { unsafe { ::std::mem::zeroed() } }
}


#[repr(C)]
#[derive(Copy)]
pub struct VkImageSubresourceRange {
    pub aspectMask: VkImageAspectFlags,
    pub baseMipLevel: uint32_t,
    pub levelCount: uint32_t,
    pub baseArrayLayer: uint32_t,
    pub layerCount: uint32_t,
}
impl Clone for VkImageSubresourceRange {
    fn clone(&self) -> Self { *self }
}
impl Default for VkImageSubresourceRange {
    fn default() -> Self { unsafe { ::std::mem::zeroed() } }
}

#[repr(C)]
#[derive(Copy)]
pub struct VkImageViewCreateInfo {
    pub sType: VkStructureType,
    pub pNext: *const ::std::os::raw::c_void,
    pub flags: VkImageViewCreateFlags,
    pub image: VkImage,
    pub viewType: VkImageViewType,
    pub format: VkFormat,
    pub components: VkComponentMapping,
    pub subresourceRange: VkImageSubresourceRange,
}
impl Clone for VkImageViewCreateInfo {
    fn clone(&self) -> Self { *self }
}
impl Default for VkImageViewCreateInfo {
    fn default() -> Self { unsafe { ::std::mem::zeroed() } }
}

#[repr(C)]
#[derive(Copy)]
pub struct VkImageSubresourceLayers {
    pub aspectMask: VkImageAspectFlags,
    pub mipLevel: uint32_t,
    pub baseArrayLayer: uint32_t,
    pub layerCount: uint32_t,
}
impl Clone for VkImageSubresourceLayers {
    fn clone(&self) -> Self { *self }
}
impl Default for VkImageSubresourceLayers {
    fn default() -> Self { unsafe { ::std::mem::zeroed() } }
}

// These could be commands?
// maybe this belongs in command_buffer?
#[repr(C)]
#[derive(Copy)]
pub struct VkImageCopy {
    pub srcSubresource: VkImageSubresourceLayers,
    pub srcOffset: VkOffset3D,
    pub dstSubresource: VkImageSubresourceLayers,
    pub dstOffset: VkOffset3D,
    pub extent: VkExtent3D,
}
impl Clone for VkImageCopy {
    fn clone(&self) -> Self { *self }
}
impl Default for VkImageCopy {
    fn default() -> Self { unsafe { ::std::mem::zeroed() } }
}

#[repr(C)]
#[derive(Copy)]
pub struct VkImageBlit {
    pub srcSubresource: VkImageSubresourceLayers,
    pub srcOffsets: [VkOffset3D; 2usize],
    pub dstSubresource: VkImageSubresourceLayers,
    pub dstOffsets: [VkOffset3D; 2usize],
}
impl Clone for VkImageBlit {
    fn clone(&self) -> Self { *self }
}
impl Default for VkImageBlit {
    fn default() -> Self { unsafe { ::std::mem::zeroed() } }
}

#[repr(C)]
#[derive(Copy)]
pub struct VkBufferImageCopy {
    pub bufferOffset: VkDeviceSize,
    pub bufferRowLength: uint32_t,
    pub bufferImageHeight: uint32_t,
    pub imageSubresource: VkImageSubresourceLayers,
    pub imageOffset: VkOffset3D,
    pub imageExtent: VkExtent3D,
}
impl Clone for VkBufferImageCopy {
    fn clone(&self) -> Self { *self }
}
impl Default for VkBufferImageCopy {
    fn default() -> Self { unsafe { ::std::mem::zeroed() } }
}

#[repr(C)]
#[derive(Copy)]
pub struct VkImageResolve {
    pub srcSubresource: VkImageSubresourceLayers,
    pub srcOffset: VkOffset3D,
    pub dstSubresource: VkImageSubresourceLayers,
    pub dstOffset: VkOffset3D,
    pub extent: VkExtent3D,
}
impl Clone for VkImageResolve {
    fn clone(&self) -> Self { *self }
}
impl Default for VkImageResolve {
    fn default() -> Self { unsafe { ::std::mem::zeroed() } }
}

pub const VK_IMAGE_LAYOUT_BEGIN_RANGE: VkImageLayout = VkImageLayout::VK_IMAGE_LAYOUT_UNDEFINED;
pub const VK_IMAGE_LAYOUT_END_RANGE: VkImageLayout = VkImageLayout::VK_IMAGE_LAYOUT_PREINITIALIZED;

#[repr(C)]
#[derive(Copy)]
pub struct VkBufferCopy {
    pub srcOffset: VkDeviceSize,
    pub dstOffset: VkDeviceSize,
    pub size: VkDeviceSize,
}
impl Clone for VkBufferCopy {
    fn clone(&self) -> Self { *self }
}
impl Default for VkBufferCopy {
    fn default() -> Self { unsafe { ::std::mem::zeroed() } }
}

#[repr(C)]
#[derive(Copy)]
pub struct VkSubresourceLayout {
    pub offset: VkDeviceSize,
    pub size: VkDeviceSize,
    pub rowPitch: VkDeviceSize,
    pub arrayPitch: VkDeviceSize,
    pub depthPitch: VkDeviceSize,
}
impl Clone for VkSubresourceLayout {
    fn clone(&self) -> Self { *self }
}
impl Default for VkSubresourceLayout {
    fn default() -> Self { unsafe { ::std::mem::zeroed() } }
}

#[repr(C)]
#[derive(Copy)]
pub struct VkBufferViewCreateInfo {
    pub sType: VkStructureType,
    pub pNext: *const ::std::os::raw::c_void,
    pub flags: VkBufferViewCreateFlags,
    pub buffer: VkBuffer,
    pub format: VkFormat,
    pub offset: VkDeviceSize,
    pub range: VkDeviceSize,
}
impl Clone for VkBufferViewCreateInfo {
    fn clone(&self) -> Self { *self }
}
impl Default for VkBufferViewCreateInfo {
    fn default() -> Self { unsafe { ::std::mem::zeroed() } }
}

#[repr(C)]
#[derive(Copy)]
pub struct VkBufferCreateInfo {
    pub sType: VkStructureType,
    pub pNext: *const ::std::os::raw::c_void,
    pub flags: VkBufferCreateFlags,
    pub size: VkDeviceSize,
    pub usage: VkBufferUsageFlags,
    pub sharingMode: VkSharingMode,
    pub queueFamilyIndexCount: uint32_t,
    pub pQueueFamilyIndices: *const uint32_t,
}
impl Clone for VkBufferCreateInfo {
    fn clone(&self) -> Self { *self }
}
impl Default for VkBufferCreateInfo {
    fn default() -> Self { unsafe { ::std::mem::zeroed() } }
}

#[derive(Clone, Copy)]
#[repr(u32)]
pub enum VkBufferCreateFlagBits {
    VK_BUFFER_CREATE_SPARSE_BINDING_BIT = 1,
    VK_BUFFER_CREATE_SPARSE_RESIDENCY_BIT = 2,
    VK_BUFFER_CREATE_SPARSE_ALIASED_BIT = 4,
}

pub type VkBufferCreateFlags = VkFlags;

#[derive(Clone, Copy)]
#[repr(u32)]
pub enum VkBufferUsageFlagBits {
    VK_BUFFER_USAGE_TRANSFER_SRC_BIT = 1,
    VK_BUFFER_USAGE_TRANSFER_DST_BIT = 2,
    VK_BUFFER_USAGE_UNIFORM_TEXEL_BUFFER_BIT = 4,
    VK_BUFFER_USAGE_STORAGE_TEXEL_BUFFER_BIT = 8,
    VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT = 16,
    VK_BUFFER_USAGE_STORAGE_BUFFER_BIT = 32,
    VK_BUFFER_USAGE_INDEX_BUFFER_BIT = 64,
    VK_BUFFER_USAGE_VERTEX_BUFFER_BIT = 128,
    VK_BUFFER_USAGE_INDIRECT_BUFFER_BIT = 256,
}

#[repr(C)]
#[derive(Copy)]
pub struct VkSparseImageFormatProperties {
    pub aspectMask: VkImageAspectFlags,
    pub imageGranularity: VkExtent3D,
    pub flags: VkSparseImageFormatFlags,
}
impl Clone for VkSparseImageFormatProperties {
    fn clone(&self) -> Self { *self }
}
impl Default for VkSparseImageFormatProperties {
    fn default() -> Self { unsafe { ::std::mem::zeroed() } }
}

#[derive(Clone, Copy)]
#[repr(u32)]
pub enum VkSparseImageFormatFlagBits {
    VK_SPARSE_IMAGE_FORMAT_SINGLE_MIPTAIL_BIT = 1,
    VK_SPARSE_IMAGE_FORMAT_ALIGNED_MIP_SIZE_BIT = 2,
    VK_SPARSE_IMAGE_FORMAT_NONSTANDARD_BLOCK_SIZE_BIT = 4,
}

pub type VkSparseImageFormatFlags = VkFlags;
