extern crate x11_dl;

use std::ptr::{
	null,
	null_mut
};

use super::utils;
use super::ffi;


pub struct VkPhysicalDevice {
	pub device: Box<ffi::physical_device::VkPhysicalDevice>
}

pub struct VkInstance {
	pub instance: Box<ffi::instance::VkInstance>
}

impl VkInstance {
	pub fn create() -> Option<VkInstance> {
		return match utils::init_instance("some_app", vec![], vec![]) {
			Ok(instance) => Some(VkInstance{ instance: instance }),
			Err(result) => panic!("Unable to create VkInstance: {}", result as u32)
		}
	}

	pub fn enumerate_physical_devices( &self ) -> Option<Vec<VkPhysicalDevice>> {

		let mut devices: [ffi::physical_device::VkPhysicalDevice;10] = [null_mut();10];
		let device_count = Box::new(devices.len() as u32);
		let burp = Box::into_raw(device_count);
		let result = unsafe {
			ffi::instance::vkEnumeratePhysicalDevices(*self.instance, burp, devices.as_mut_ptr() )
		};

		let merp = unsafe{ Box::from_raw(burp) };
		drop(merp);

		if result as i32 == ffi::VkResult::VK_SUCCESS as i32 {
			let mut v = Vec::new();
			for x in devices.iter() {
				v.push( VkPhysicalDevice{ device: Box::new(*x) } );
			}
			return Some(v);
		}
		return None;
	}
}

impl Drop for VkInstance {
	// All child objects created using instance must have been destroyed prior to destroying instance
	// - can we keep track of those objects in VkInstance?
	fn drop(&mut self) {
		unsafe {
			ffi::instance::vkDestroyInstance( *self.instance, null() )
		}
	}
}

#[test]
fn vkinstance_enumerates_physical_devices() {
	match VkInstance::create() {
		Some(instance) => {
			match instance.enumerate_physical_devices() {
				Some(devices) => assert!(devices.len() > 0),
				None => {
					assert!(false);
				}
			}
		},
		None => {
			assert!(false);
		}
	}
}

#[test]
fn create_instance_returns_some() {
	match VkInstance::create() {
		Some(..) => (),
		None => {
			println!("No instance returned.");
		}
	}
}

// Tests

#[test]
#[allow(non_snake_case)]
fn vkCreateInstance_should_be_callable() {
	let mut info = ffi::instance::VkInstanceCreateInfo::default();
	info.sType = ffi::VkStructureType::VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	unsafe {
		let rawInfo = &info as *const ffi::instance::VkInstanceCreateInfo;
		let mut instance: ffi::instance::VkInstance = null_mut();
		let result = ffi::instance::vkCreateInstance(rawInfo, null(), &mut instance);
		assert!( result as i32 == ffi::VkResult::VK_SUCCESS as i32);
	}
}


