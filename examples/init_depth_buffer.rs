extern crate vulkan;
extern crate x11_dl;

use vulkan::utils;
use vulkan::ffi;
use vulkan::xlib_wrappers;

use std::ptr::{
	null_mut
};


fn main() {
	let layer_props = utils::instance_layer_properties();
	let instance_extension_names = vec![
		ffi::VK_KHR_SURFACE_EXTENSION_NAME,
		ffi::VK_KHR_XLIB_SURFACE_EXTENSION_NAME
	];
	let device_extension_names = vec![ffi::VK_KHR_DISPLAY_SWAPCHAIN_EXTENSION_NAME];
	let instance = 
		utils::init_instance(
			"init_swap_chain", 
			vec![],
			instance_extension_names
	).expect("Unable to create instance");
	let xlib_window = xlib_wrappers::XlibWindow::create(800, 600, "init_depth_buffer");
	let surface = utils::create_xlib_surface_khr(&instance, &xlib_window).expect("Unable to create surface"); //rc?
	let physical_devices = utils::enumerate_physical_devices(&instance);
	let mut memory_properties = utils::get_physical_device_memory_properties(&physical_devices[0]);
	let mut physical_device_properties = utils::get_physical_device_properties(&physical_devices[0]);
	let device = utils::create_device(&physical_devices[0]).expect("Unable to create device!");
	let suface_capabilities = utils::get_surface_capabilities(&physical_devices[0], &surface).expect("Unable to get surface capabilities: ");
	let swap_chain_count:u32 = 2; // arg
	let format = ffi::VkFormat::VK_FORMAT_B8G8R8A8_UNORM;
	println!("creating swapchain");
	let swapchain = 
		utils::create_swapchain_khr(&device, &surface, format, swap_chain_count).expect("Unable to create swapchain"); 
	println!("creating swapchain images");
	let swapchain_images = 
		utils::get_swapchain_images_khr(&device, &swapchain).expect("Unable to create swapchain images: ");
	let cmd_pool = utils::create_command_pool(&device, 0).expect("could not create command pool");
	let cmd_buffer = utils::create_command_buffer(&device, &cmd_pool).expect("Could not create command buffer");
	utils::execute_begin_command_buffer(&cmd_buffer);
	println!("get queue");
	let graphics_queue_family_index = 0;
	let queue = utils::get_device_queue(&device, graphics_queue_family_index);
	println!("create image views");
	let image_views = utils::create_swapchain_image_views(&device, &cmd_buffer, &swapchain_images, format).expect("Unable to create imageviews");

	// 
	let image = create_image(
		800, 600,
		ffi::resource::VkImageType::VK_IMAGE_TYPE_2D,
		ffi::VkFormat::VK_FORMAT_D16_UNORM,
		ffi::resource::VkImageTiling::VK_IMAGE_TILING_LINEAR,
	).expect("Unable to create image");
	
	
	println!("ending command buffer");
	utils::execute_end_command_buffer(&cmd_buffer);
	let buffers = vec![cmd_buffer];
	println!("queue command buffer");
	utils::execute_queue_command_buffer(&device, &queue, &buffers);
	
	println!("mapping window");
	xlib_window.map_window();
	println!("starting event loop");
	xlib_window.event_loop();

	println!("cleaning up");
	utils::free_command_buffers(&device, &cmd_pool, &vec![cmd_buffer]);
	utils::destroy_command_pool(&device, &cmd_pool);
	utils::destroy_instance(&instance);
}


pub fn create_image(
	width:u32,
	height:u32,
	imageType: ffi::resource::VkImageType,
	format: ffi::VkFormat,
	tiling: ffi::resource::VkImageTiling,
) -> Result<ffi::resource::VkImage, ffi::VkResult> {

	let mut image_info = ffi::resource::VkImageCreateInfo::default();
	let format = ffi::VkFormat::VK_FORMAT_D16_UNORM;
	//TODO: detect physical device support as per tutorial
	image_info.tiling = tiling;
	image_info.sType = ffi::VkStructureType::VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
	image_info.pNext = null_mut();
	image_info.imageType = imageType;
	image_info.format = format;
	image_info.extent.width = width;
	image_info.extent.height = height;
	image_info.extent.depth = 1;
	image_info.mipLevels = 1;
	image_info.arrayLayers = 1;
	image_info.samples = ffi::VkSampleCountFlagBits::VK_SAMPLE_COUNT_1_BIT; //NUM_SAMPLES
	image_info.initialLayout = ffi::resource::VkImageLayout::VK_IMAGE_LAYOUT_UNDEFINED;
	image_info.usage = ffi::resource::VkImageUsageFlagBits::VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT as u32; // TODO: fix image info?
	image_info.queueFamilyIndexCount = 0;
	image_info.pQueueFamilyIndices = null_mut();
	image_info.sharingMode = ffi::VkSharingMode::VK_SHARING_MODE_EXCLUSIVE;
	image_info.flags = 0;

	// TODO: extract create_memory_info
	let mut memory_info = ffi::memory::VkMemoryAllocateInfo::default();
	memory_info.sType = ffi::VkStructureType::VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	memory_info.pNext = null_mut();
	memory_info.allocationSize = 0;
	memory_info.memoryTypeIndex = 0;

	// TODO: extract create_image_view
	let mut image_view_info = ffi::resource::VkImageViewCreateInfo {
		sType : ffi::VkStructureType::VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
		pNext : null_mut(),
		image : null_mut(),
		format : format,
		components : ffi::VkComponentMapping {
			r : ffi::VkComponent::VK_COMPONENT_SWIZZLE_R,
			g : ffi::VkComponent::VK_COMPONENT_SWIZZLE_G,
			b : ffi::VkComponent::VK_COMPONENT_SWIZZLE_B,
			a : ffi::VkComponent::VK_COMPONENT_SWIZZLE_A,
		},
		subresourceRange : "",
		viewType : ffi::resource::VkImageViewType::VK_IMAGE_VIEW_TYPE_2D,
		flags : 0
	};



}
