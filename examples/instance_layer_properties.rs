extern crate vulkan;

use vulkan::utils;

use std::ptr;
use vulkan::ffi;

fn main() {

	let props = utils::instance_layer_properties().expect("Unable to enumerate layer properties");
	if props.len() > 0 {
		println!("Found {} layer properties", props.len());
		for ext in props {
			// Ought to implement Display and Debug for Vulkan types

			// &[i8;256] -> &[u8;256] -> &str
			let s: &[u8;256] = unsafe { std::mem::transmute(&ext.layerName) };
			let ss = std::str::from_utf8(s);
			let name = ss.unwrap();

			println!("Layer name: {}", name);

			let bytes: &[u8;256] = unsafe { std::mem::transmute(&ext.description) };
			let description = std::str::from_utf8(s).unwrap();
			println!("Description: {}", description);
		}
	} else {
		println!("No props found.");
	}

}

