extern crate vulkan;

use vulkan::ffi;
use std::ptr;
use vulkan::utils;

fn main() {

	let maybe_instance = utils::init_instance("some lowly app", vec![], vec![]);

	match maybe_instance {
		Ok(instance) => {
			println!("Created instance");
			unsafe {
				ffi::instance::vkDestroyInstance(*instance, ptr::null())
			};
			println!("Destroyed instance.");
		},
		Err(result) => {
			println!("Error creating instance, VkResult: {}", result as i32 );
		}
	}

}
