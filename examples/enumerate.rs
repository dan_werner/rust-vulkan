extern crate vulkan;

use vulkan::ffi;
use std::ptr;

use vulkan::ffi::physical_device::VkPhysicalDevice;
use vulkan::utils;

fn main() {

	let instance = utils::init_instance("some lowly app", vec![], vec![]).ok().unwrap();

	let mut gpu_count = 0;
	unsafe {
		ffi::instance::vkEnumeratePhysicalDevices(*instance, &mut gpu_count, ptr::null_mut())
	};

	let gpus = utils::enumerate_physical_devices(&instance);

	println!("Expected {}, found {} physical devices.", gpu_count, gpus.len() );

	unsafe {
		ffi::instance::vkDestroyInstance(*instance, ptr::null())
	};
	
}
