extern crate vulkan;

use vulkan::utils;

fn main() {
	let instance = utils::init_instance("app_name", vec![], vec![]).ok().unwrap();
	let devices = utils::enumerate_physical_devices(&instance);

	let gpu = devices[0];

	let device = utils::create_device(&gpu).ok().unwrap();
	utils::destroy_device(&device);
	
	utils::destroy_instance(&instance);
}


