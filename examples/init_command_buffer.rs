extern crate vulkan;
extern crate x11_dl;

use vulkan::utils;
use vulkan::ffi;
use vulkan::xlib_wrappers;

fn main() {
	let layer_props = utils::instance_layer_properties();
	let instance_extension_names = vec![
		ffi::VK_KHR_SURFACE_EXTENSION_NAME,
		ffi::VK_KHR_XLIB_SURFACE_EXTENSION_NAME
	];
	let device_extension_names = vec![ffi::VK_KHR_DISPLAY_SWAPCHAIN_EXTENSION_NAME];

	let instance = 
		utils::init_instance(
			"commandbuffer sample", 
			vec![],
			instance_extension_names
	).expect("Unable to create instance");

	let physical_devices = utils::enumerate_physical_devices(&instance);

	let mut memory_properties = utils::get_physical_device_memory_properties(&physical_devices[0]);
	let mut physical_device_properties = utils::get_physical_device_properties(&physical_devices[0]);
	
	let device = utils::create_device(&physical_devices[0]).expect("Unable to create device!");
	let cmd_pool = utils::create_command_pool(&device, 0).expect("could not create command pool");
	let cmd_buffer = utils::create_command_buffer(&device, &cmd_pool).expect("Could not create command buffer");
	let xlib_window = xlib_wrappers::XlibWindow::create(800, 600, "TITLE_HERE_PLZ");
	let surface = utils::create_xlib_surface_khr(&instance, &xlib_window); //rc?
	
	xlib_window.event_loop();

	utils::free_command_buffers(&device, &cmd_pool, &vec![cmd_buffer]);
	utils::destroy_command_pool(&device, &cmd_pool);
	utils::destroy_instance(&instance);
}

