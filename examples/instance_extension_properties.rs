extern crate vulkan;

use vulkan::utils;

fn main() {
	let props = utils::instance_extension_properties().ok().unwrap();
	if props.len() > 0 {
		println!("Found {} extension properties", props.len());
		for ext in props {
			// Ought to implement Display and Debug for Vulkan types

			// &[i8;256] -> &[u8;256] -> &str
			let s: &[u8;256] = unsafe { std::mem::transmute(&ext.extensionName) };
			let ss = std::str::from_utf8(s);
			let name = ss.unwrap();

			println!("Extension name: {}", name);
			println!("Spec version: {}", ext.specVersion);
		}
	} else {
		println!("No props found.");
	}
}

