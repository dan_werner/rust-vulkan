extern crate vulkan;
extern crate x11_dl;

use vulkan::utils;
use vulkan::ffi;
use vulkan::xlib_wrappers;

use std::ptr::{
	null_mut
};


fn main() {
	let layer_props = utils::instance_layer_properties();
	let instance_extension_names = vec![
		ffi::VK_KHR_SURFACE_EXTENSION_NAME,
		ffi::VK_KHR_XLIB_SURFACE_EXTENSION_NAME
	];
	let device_extension_names = vec![ffi::VK_KHR_DISPLAY_SWAPCHAIN_EXTENSION_NAME];

	let instance = 
		utils::init_instance(
			"init_swap_chain", 
			vec![],
			instance_extension_names
	).expect("Unable to create instance");

	let xlib_window = xlib_wrappers::XlibWindow::create(800, 600, "init_swap_chain");
	let surface = utils::create_xlib_surface_khr(&instance, &xlib_window).expect("Unable to create surface"); //rc?

	let physical_devices = utils::enumerate_physical_devices(&instance);

	let mut memory_properties = utils::get_physical_device_memory_properties(&physical_devices[0]);
	let mut physical_device_properties = utils::get_physical_device_properties(&physical_devices[0]);
	
	let device = utils::create_device(&physical_devices[0]).expect("Unable to create device!");
	let suface_capabilities = utils::get_surface_capabilities(&physical_devices[0], &surface).expect("Unable to get surface capabilities: ");

	let swap_chain_count:u32 = 2; // arg
	let format = ffi::VkFormat::VK_FORMAT_B8G8R8A8_UNORM;

	println!("creating swapchain");
	let swapchain = 
		utils::create_swapchain_khr(&device, &surface, format, swap_chain_count).expect("Unable to create swapchain"); 

	println!("creating swapchain images");
	let swapchain_images = 
		utils::get_swapchain_images_khr(&device, &swapchain).expect("Unable to create swapchain images: ");

	let cmd_pool = utils::create_command_pool(&device, 0).expect("could not create command pool");
	let cmd_buffer = utils::create_command_buffer(&device, &cmd_pool).expect("Could not create command buffer");
	utils::execute_begin_command_buffer(&cmd_buffer);


	println!("get queue");
	let graphics_queue_family_index = 0;
	let queue = utils::get_device_queue(&device, graphics_queue_family_index);

	println!("create image views");
	let image_views = utils::create_swapchain_image_views(&device, &cmd_buffer, &swapchain_images, format).expect("Unable to create imageviews");

	println!("ending command buffer");
	utils::execute_end_command_buffer(&cmd_buffer);
	let buffers = vec![cmd_buffer];
	println!("queue command buffer");
	utils::execute_queue_command_buffer(&device, &queue, &buffers);
	
	println!("mapping window");
	xlib_window.map_window();
	println!("starting event loop");
	xlib_window.event_loop();

	println!("cleaning up");
	utils::free_command_buffers(&device, &cmd_pool, &vec![cmd_buffer]);
	utils::destroy_command_pool(&device, &cmd_pool);
	utils::destroy_instance(&instance);
}

